-- Tabla de metabolito
DROP TABLE IF EXISTS metabolito CASCADE;

CREATE TABLE metabolito (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR,
	smiles VARCHAR,
	formula VARCHAR,
	peso_mol REAL
);

-- Tabla de gen
DROP TABLE IF EXISTS gen CASCADE;

CREATE TABLE gen (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR,
	seq VARCHAR,
	cromosoma INTEGER,
	locus VARCHAR
);

-- Tabla de enzima
DROP TABLE IF EXISTS enzima CASCADE;

CREATE TABLE enzima (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR,
	seq VARCHAR,
	idGen INTEGER,
	FOREIGN KEY (idGen) REFERENCES gen(id)
);

-- Tabla ruta
DROP TABLE IF EXISTS ruta CASCADE;

CREATE TABLE ruta (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR NOT NULL,
	idsmpdb INTEGER NOT NULL,
	data_svg VARCHAR
);

-- Tabla de asociaciones de enzimas con metabolito
DROP TABLE IF EXISTS enz_metabolito CASCADE;

CREATE TABLE enz_metabolito (
	id SERIAL PRIMARY KEY,
	idEnz INTEGER,
	idMet INTEGER,
	FOREIGN KEY (idEnz) REFERENCES enzima(id),
	FOREIGN KEY (idMet) REFERENCES metabolito(id)
);


-- Tabla de asociaciones de rutas con metabolito
DROP TABLE IF EXISTS ruta_metabolito CASCADE;

CREATE TABLE ruta_metabolito (
	id SERIAL PRIMARY KEY,
	idRuta INTEGER,
	idMet INTEGER,
	FOREIGN KEY (idRuta) REFERENCES ruta(id),
	FOREIGN KEY (idMet) REFERENCES metabolito(id)
);

-- Tabla de las asociaciones de rutas con enzima
DROP TABLE IF EXISTS ruta_enzima CASCADE;

CREATE TABLE ruta_enzima (
	id SERIAL PRIMARY KEY,
	idEnz INTEGER,
	idRuta INTEGER,
	FOREIGN KEY (idEnz) REFERENCES enzima(id),
	FOREIGN KEY (idRuta) REFERENCES ruta(id)
);

-- Hacer que los nombres sean unicos
ALTER TABLE ruta ADD CONSTRAINT nombre_ruta_unico UNIQUE (nombre);
ALTER TABLE metabolito ADD CONSTRAINT nombre_met_unico UNIQUE (nombre);
ALTER TABLE gen ADD CONSTRAINT nombre_gen_unico UNIQUE (nombre);
ALTER TABLE enzima ADD CONSTRAINT nombre_enzima_unico UNIQUE (nombre);
ALTER TABLE ruta ADD CONSTRAINT idsmpdb_unica UNIQUE (idsmpdb);

-- Procedimientos almacenados

-- Actualizar el peso molecular de un metabolito
CREATE OR REPLACE FUNCTION actualizar_peso_mol(valor REAL, nombre_met VARCHAR)
RETURNS VOID AS $$
BEGIN
	UPDATE metabolito SET peso_mol = valor WHERE nombre = nombre_met;
END;
$$ LANGUAGE plpgsql;

-- Seleccionar cuantos genes hay por cromosoma
CREATE OR REPLACE FUNCTION cant_por_cromosoma()
RETURNS TABLE (cromosoma_gen INT, frecuencia_gen BIGINT) AS $$
BEGIN
	RETURN QUERY SELECT cromosoma, COUNT(*) AS frecuencia FROM gen GROUP BY cromosoma ORDER BY cromosoma;
END;
$$ LANGUAGE plpgsql;

-- Seleccionar genes junto a sus enzimas
CREATE OR REPLACE FUNCTION reporte_gen_enz()
RETURNS TABLE (id_gen INT, nombre_gen VARCHAR, nombre_enzima VARCHAR) AS $$
BEGIN
	RETURN QUERY SELECT gen.id, gen.nombre, enzima.nombre FROM gen INNER JOIN enzima ON gen.id = enzima.idgen;
END;
$$ LANGUAGE plpgsql;

-- Eliminar las enzimas con gen desconocido
CREATE OR REPLACE FUNCTION limpiar_enzimas()
RETURNS VOID AS $$
BEGIN
	DELETE FROM enz_metabolito WHERE idenz IN (SELECT id FROM enzima WHERE idgen IS NULL);
	DELETE FROM ruta_enzima WHERE idenz IN (SELECT id FROM enzima WHERE idgen IS NULL);
	DELETE FROM enzima WHERE idgen IS NULL;
END;
$$ LANGUAGE plpgsql;

-- Conteo de X por Y en una tabla intermedia Z (rutas por metabolito, metabolitos por ruta, metabolitos por enzima, enzimas por metabolito, enzimas por ruta, rutas por enzima)
CREATE OR REPLACE FUNCTION cant_a_por_b(nombre_tabla VARCHAR, idbase VARCHAR, idobj INT)
RETURNS NUMERIC AS $$
DECLARE 
    cantidad NUMERIC;
BEGIN
    EXECUTE 'SELECT COUNT(*) FROM ' || nombre_tabla || ' WHERE ' || idbase || ' = ' || idobj INTO cantidad USING idobj;
    
    RETURN cantidad;
END;
$$ LANGUAGE plpgsql;

-- Composicion completa de una ruta metabolica
CREATE OR REPLACE FUNCTION comp_ruta(id_ruta INT)
RETURNS TABLE (nombre_ruta VARCHAR, nombre_met VARCHAR, nombre_enz VARCHAR) AS $$
BEGIN
	RETURN QUERY SELECT ruta.nombre AS nombre_ruta,
	metabolito.nombre AS nombre_metabolito,
	enzima.nombre AS nombre_enzima FROM ruta
	INNER JOIN
	ruta_metabolito ON ruta.id = ruta_metabolito.idruta
	INNER JOIN
	metabolito ON ruta_metabolito.idmet = metabolito.id
	INNER JOIN
	ruta_enzima ON ruta.id = ruta_enzima.idruta	
	INNER JOIN
	enzima ON ruta_enzima.idenz = enzima.id WHERE ruta.id = id_ruta;
END;
$$ LANGUAGE plpgsql;
