CREATE TABLE "mascota"(
    "id" INTEGER NOT NULL,
    "nombre" VARCHAR(255) NOT NULL,
    "especie" VARCHAR(255) NOT NULL
);
ALTER TABLE
    "mascota" ADD PRIMARY KEY("id");
CREATE TABLE "persona"(
    "id" INTEGER NOT NULL,
    "nombre" VARCHAR(255) NOT NULL,
    "idMascotas" INTEGER NOT NULL
);
ALTER TABLE
    "persona" ADD PRIMARY KEY("id");
ALTER TABLE
    "persona" ADD CONSTRAINT "persona_idmascotas_foreign" FOREIGN KEY("idMascotas") REFERENCES "mascota"("id");