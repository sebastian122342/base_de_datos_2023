# alimentar la tabla de las enzimas vienen de un archivo fasta

from Bio import SeqIO
import psycopg2

def main():
    # conexion a la bdd
    conn = psycopg2.connect(database = "prebase", user="postgres", password="122342", host="localhost", port="5432")
    cursor = conn.cursor()
    
    path = "/home/seba/Downloads/datos_bdd/protein.fasta"

    for record in SeqIO.parse(path, "fasta"):
        title = record.description
        name = title.lstrip(record.id).strip(" ")
        seq = str(record.seq)

        cursor.execute("SELECT id FROM gen WHERE nombre = %s", (name,))
        idgen = cursor.fetchone()
        
        insert_query = f"INSERT INTO enzima (nombre, seq, idgen) VALUES (%s, %s, %s)"
        data = (name, seq, idgen[0] if idgen is not None else None)

        cursor.execute(insert_query, data)
        conn.commit()

if __name__ == "__main__":
    main()
