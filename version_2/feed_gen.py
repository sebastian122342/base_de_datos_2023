# alimentar la tabla de las enzimas vienen de un archivo fasta

from Bio import SeqIO
import psycopg2

def main():
    # conexion a la bdd
    conn = psycopg2.connect(database = "prebase", user="postgres", password="122342", host="localhost", port="5432")
    cursor = conn.cursor()
    
    path = "/home/seba/Downloads/datos_bdd/gene.fasta"

    for record in SeqIO.parse(path, "fasta"):
        title = record.description
        name = title.lstrip(record.id).strip(" ")
        seq = str(record.seq)

        #print(name)
        
        insert_query = "INSERT INTO gen (nombre, seq) VALUES (%s, %s)"
        data = (name, seq)
        
        cursor.execute(insert_query, data)
        conn.commit()

if __name__ == "__main__":
    main()
