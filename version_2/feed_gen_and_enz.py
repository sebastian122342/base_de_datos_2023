# alimentar enzimas y genes a la bdd

import psycopg2
import xml.etree.ElementTree as et
from Bio import SeqIO
from io import StringIO

def main():

    print("excelentes tardes")
    # parsear el xml
    path = "/home/seba/Downloads/datos_bdd/hmdb_proteins.xml"
    tree = et.parse(path)
    root = tree.getroot()

    # conexion a la bdd
    conn = psycopg2.connect(database = "bdd_versiondos", user="postgres", password="122342", host="localhost", port="5432")
    cursor = conn.cursor()

    print(root)
    
    for prot in root.findall("{http://www.hmdb.ca}protein"):

        # sacar los datos
        ptype = prot.find("{http://www.hmdb.ca}protein_type").text
        if ptype == "Enzyme":
            #print(ptype)
            prot_prop = prot.find("{http://www.hmdb.ca}protein_properties")
            prot_seq = prot_prop.find("{http://www.hmdb.ca}polypeptide_sequence").text
            # comprobar que no sea none
            if prot_seq:
                fasta_file = StringIO(prot_seq)
                for record in SeqIO.parse(fasta_file, "fasta"):
                    enz_name = record.description
                    prot_seq = str(record.seq)
                
            gen_name = prot.find("{http://www.hmdb.ca}gene_name").text
            gen_prop = prot.find("{http://www.hmdb.ca}gene_properties")
            gen_seq = gen_prop.find("{http://www.hmdb.ca}gene_sequence").text
            if gen_seq:
                fasta_file = StringIO(gen_seq)
                for record in SeqIO.parse(fasta_file, "fasta"):
                    gen_seq = str(record.seq)
            # chr_loc = gen_prop.find("{http://www.hmdb.ca}chromosome_location").text
            # locus = gen_prop.find("{{http://www.hmdb.ca}polypeptide_sequence}locus").text

            #print(f"Gen-> secuencia {gen_seq}\tnombre {gen_name}")
            #print(f"Enzima-> secuencia {prot_seq}\tnombre {enz_name}, sacar id del gen")

            # preparar la insercion, primero gen
            insert_query = "INSERT INTO gen (nombre, seq) VALUES (%s, %s)"
            data = (gen_name, gen_seq)

            # insertar y commitear
            try:
                cursor.execute(insert_query, data)
                conn.commit()
            except:
                conn.rollback()

            # Segunda insercion, en tabla enzima
            cursor.execute("SELECT id FROM gen WHERE nombre = %s", (gen_name,))
            idgen = cursor.fetchone()

            insert_query = "INSERT INTO enzima (nombre, seq, idgen) VALUES (%s, %s, %s)"
            data = (enz_name, prot_seq, idgen[0] if idgen is not None else None)

            # insertar y commitear
            try:
                cursor.execute(insert_query, data)
                conn.commit()
            except:
                conn.rollback()
                
if __name__ == "__main__":
    main()
