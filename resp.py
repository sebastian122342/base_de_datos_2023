 """
    def make_int1_treeview(self, records, columns, tree):
        if tree.get_columns():
            for column in tree.get_columns():
                tree.remove_column(column)

        column_length = len(columns)
        tmodel = Gtk.ListStore(str, bool)
        tree.set_model(model=tmodel)

        celltext = Gtk.CellRendererText()
        
        column_text = Gtk.TreeViewColumn(columns[0], celltext, text=0)
        column_text.set_sort_column_id(0)
        
        column_toggle = Gtk.TreeViewColumn(columns[1], self.celltoggle1, active=1)
        column_toggle.set_sort_column_id(1)
        
        tree.append_column(column_text)
        tree.append_column(column_toggle)

        for item in records:
            text = str(item[0])
            checked = int(item[1])
            tmodel.append([text, checked])

    def make_int2_treeview(self, records, columns, tree):
        if tree.get_columns():
            for column in tree.get_columns():
                tree.remove_column(column)

        column_length = len(columns)
        tmodel = Gtk.ListStore(str, bool)
        tree.set_model(model=tmodel)

        celltext = Gtk.CellRendererText()
        
        column_text = Gtk.TreeViewColumn(columns[0], celltext, text=0)
        column_text.set_sort_column_id(0)

        
        column_toggle = Gtk.TreeViewColumn(columns[1], self.celltoggle2, active=1)
        column_toggle.set_sort_column_id(1)
        
        tree.append_column(column_text)
        tree.append_column(column_toggle)

        for item in records:
            text = str(item[0])
            checked = int(item[1])
            tmodel.append([text, checked])
    """

    """
    def on_celltoggle1_clicked(self, toggle, path):
        past = toggle.get_active()
        model = self.tree_int1.get_model()
        model[path][1] = not past
        self.tree_int1.set_model(model)
        self.btn_add.set_sensitive(True)
        self.btn_edit.set_sensitive(True)

    def on_celltoggle2_clicked(self, toggle, path):
        past = toggle.get_active()
        model = self.tree_int2.get_model()
        model[path][1] = not past
        self.tree_int2.set_model(model)
        self.btn_add.set_sensitive(True)
        self.btn_edit.set_sensitive(True)
    """
