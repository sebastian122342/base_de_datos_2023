# la idea es hacer un programa que sea capaz de dibujar rutas metabolicas a partir de la informacion del JOIN generado

import psycopg2
import graphviz

def main():

    # conexion a la bdd
    conn = psycopg2.connect(database = "nuevo_modelo", user="postgres", password="122342", host="localhost", port="5432")
    cursor = conn.cursor()

    idruta = 173
    
    # ver como se procesan las cosas
    cursor.execute("SELECT ruta.id AS idruta, ruta.nombre AS nombre_ruta, metabolito.id AS idmet, metabolito.nombre AS nombre_metabolito, enzima.id AS idenz, enzima.nombre AS nombre_enzima FROM ruta INNER JOIN ruta_metabolito ON ruta.id = ruta_metabolito.idruta INNER JOIN metabolito ON ruta_metabolito.idmet = metabolito.id INNER JOIN ruta_enzima ON ruta.id = ruta_enzima.idruta INNER JOIN enzima ON ruta_enzima.idenz = enzima.id WHERE ruta.id = %s ORDER BY enzima.id;", (idruta,))
    elements = cursor.fetchall()

    pathway = graphviz.Digraph("G", filename="hola.gv")
    # 0,      1,    2,     3,   4,     5
    # idruta, ruta, idmet, met, idenz, enz
    for regist in elements:
        pathway.edge(regist[5], regist[3])
        
    pathway.view()
        
    #print(elements)
    
    
if __name__ == "__main__":
    main()
