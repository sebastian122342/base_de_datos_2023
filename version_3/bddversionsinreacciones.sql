-- Tabla de metabolito
DROP TABLE IF EXISTS metabolito CASCADE;

CREATE TABLE metabolito (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR,
	smiles VARCHAR,
	formula VARCHAR,
	peso_mol REAL
);

-- Tabla de gen
DROP TABLE IF EXISTS gen CASCADE;

CREATE TABLE gen (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR,
	seq VARCHAR,
	cromosoma INTEGER,
	locus VARCHAR
);

-- Tabla de enzima
DROP TABLE IF EXISTS enzima CASCADE;

CREATE TABLE enzima (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR,
	seq VARCHAR,
	idGen INTEGER,
	FOREIGN KEY (idGen) REFERENCES gen(id)
);

-- Tabla ruta
DROP TABLE IF EXISTS ruta CASCADE;

CREATE TABLE ruta (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR NOT NULL
);

-- Tabla de asociaciones de enzimas con metabolito
DROP TABLE IF EXISTS enz_metabolito CASCADE;

CREATE TABLE enz_metabolito (
	id SERIAL PRIMARY KEY,
	idEnz INTEGER,
	idMet INTEGER,
	FOREIGN KEY (idEnz) REFERENCES enzima(id),
	FOREIGN KEY (idMet) REFERENCES metabolito(id)
);


-- Tabla de asociaciones de rutas con metabolito
DROP TABLE IF EXISTS ruta_metabolito CASCADE;

CREATE TABLE ruta_metabolito (
	id SERIAL PRIMARY KEY,
	idRuta INTEGER,
	idMet INTEGER,
	FOREIGN KEY (idRuta) REFERENCES ruta(id),
	FOREIGN KEY (idMet) REFERENCES metabolito(id)
);

-- Tabla de las asociaciones de rutas con enzima
DROP TABLE IF EXISTS ruta_enzima CASCADE;

CREATE TABLE ruta_enzima (
	id SERIAL PRIMARY KEY,
	idEnz INTEGER,
	idRuta INTEGER,
	FOREIGN KEY (idEnz) REFERENCES enzima(id),
	FOREIGN KEY (idRuta) REFERENCES ruta(id)
);

-- Hacer que los nombres sean unicos
ALTER TABLE ruta ADD CONSTRAINT nombre_ruta_unico UNIQUE (nombre);
ALTER TABLE metabolito ADD CONSTRAINT nombre_met_unico UNIQUE (nombre);
ALTER TABLE gen ADD CONSTRAINT nombre_gen_unico UNIQUE (nombre);
ALTER TABLE enzima ADD CONSTRAINT nombre_enzima_unico UNIQUE (nombre);
