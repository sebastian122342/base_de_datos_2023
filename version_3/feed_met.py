# alimentarle los metabolitos a la base de datos

import psycopg2
import xml.etree.ElementTree as et

def main():

    print("excelentes tardes")
    # parsear el xml
    path_met = "/home/seba/Downloads/datos_bdd/urine_metabolites.xml"
    tree = et.parse(path_met)
    root = tree.getroot()

    # conexion a la bdd
    conn = psycopg2.connect(database = "nuevo_modelo", user="postgres", password="122342", host="localhost", port="5432")
    cursor = conn.cursor()

    print(root)
   
    # todos los metabolitos dentro del xml
    for met in root.findall("{http://www.hmdb.ca}metabolite"):

        # sacar los datos
        name = met.find("{http://www.hmdb.ca}name").text
        smiles = met.find("{http://www.hmdb.ca}smiles").text
        formula = met.find("{http://www.hmdb.ca}chemical_formula").text
        try:
            mweight = float(met.find("{http://www.hmdb.ca}average_molecular_weight").text)
        except:
            mweight = None
        #print(name, mweight)
        
        # preparar la insercion
        insert_query = "INSERT INTO metabolito (nombre, smiles, formula, peso_mol) VALUES (%s, %s, %s, %s)"
        data = (name, smiles, formula, mweight)

        # insertar y commitear
        try:
            cursor.execute(insert_query, data)
            conn.commit()
        except:
            conn.rollback()

        # Añadir las rutas y su relacion con el metabolito
        bio_prop = met.find("{http://www.hmdb.ca}biological_properties")
        pathways = bio_prop.find("{http://www.hmdb.ca}pathways")
        if pathways:
            for p in pathways:
                # agregar la ruta a la tabla de rutas
                pathway = p.find("{http://www.hmdb.ca}name").text

                # limpiar el nombre de la ruta, quedarse con el nombre solamente sin notaciones de estructuras
                path_names = pathway.split(" ") # separar segun espacios
                if len(path_names) > 1:
                    pathway = path_names[0] + " " + path_names[1]
                else:
                    pathway = path_names[0]

                # ante repeticiones de rutas, considerando que lo mismo se hace cuando se insertan enzimas
                try:
                    insert_query = "INSERT INTO ruta (nombre) VALUES (%s)"
                    data = (pathway,)

                    cursor.execute(insert_query, data)
                    conn.commit()
                    print("insertada la pathway")
                except:
                    conn.rollback()

                # establecer la conexion entre la ruta y el metabolito en la tabla de rutas, aqui no se revisa no repeticion
                insert_query = "INSERT INTO ruta_metabolito (idMet, idRuta) VALUES (%s, %s)"

                # id del metabolito
                cursor.execute("SELECT id FROM metabolito WHERE nombre = %s", (name,))
                idmet = cursor.fetchone()

                # id de la ruta
                cursor.execute("SELECT id FROM ruta WHERE nombre = %s", (pathway,))
                idruta = cursor.fetchone()

                # agregar el registro a la tabla de relacion
                data = (idmet, idruta)

                cursor.execute(insert_query, data)
                conn.commit()

        # los registros de enz_metabolito se hace en el script de enzimas, ejecutado despues del de metabolito

if __name__ == "__main__":
    main()
