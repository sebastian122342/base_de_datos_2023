# About Dialog

# Se importan los modulos necesarios
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

authors = ["Sebastián Bustamante", "Bastián Morales"]
comment = "Tercera guia unidad 2, Programacion Avanzada"
program_name = "Catalogo de hoteles"
icono_stock = "applications-internet"

class AboutDialog(Gtk.AboutDialog):
    """ Ventana de dialogo con metodos para mostrar informacion necesaria """
    def __init__(self):
        super().__init__(title="About Us")
        self.set_modal(True)
        self.add_credit_section("Autores", authors)
        self.set_comments(comment)
        self.set_logo_icon_name(icono_stock)
        self.set_program_name(program_name)
        self.set_version("1.0")

        self.show_all()
