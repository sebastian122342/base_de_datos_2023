# Main

import matplotlib.pyplot as plt

# Se importan los modulos necesarios
from disease import Disease
from person import Person
from community import Community
from simulator import Simulator

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf

from about_dialog import AboutDialog
from dialog_select import DialogSelect

from messages import on_info_clicked

def is_number(string):
    """chequea si una string representa algun numero"""
    try:
        float(string)
        return True
    except ValueError:
        return False

"""
Se hace una simulacion de la propagacion de una enfermedad, en una poblacion
determinada.

Esta simulacion se hace apegandose al modelo SIR.
Sin embargo hay algunas modificaciones, hay vacunas, conteo de muertos,
enfermedades y condiciones de base y edades, que permiten que no todas las
personas sean igual de susceptibles ante la enfermedad.
"""

class MainWindow(Gtk.Window):
    def __init__(self):
        super().__init__()
        self.set_default_size(600, 600)
        self.set_border_width(20)
        self.connect("destroy", Gtk.main_quit)

        # Se crea la grid que contendrá los demas widgets
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        # Se crea la headerbar
        self.hb = Gtk.HeaderBar()
        self.hb.set_title("Coso ploteado")
        self.hb.set_subtitle("siiiiiiiii")
        self.hb.set_show_close_button(True)
        self.set_titlebar(self.hb)

        # Boton Ventana About
        self.btn_about = Gtk.Button()
        icon = Gio.ThemedIcon(name="help-about")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_about.add(image)
        self.btn_about.connect("clicked", self.on_btn_about)
        self.hb.pack_end(self.btn_about)

        # Boton comodo para datos predeterminados
        self.btn_quick = Gtk.Button()
        icon = Gio.ThemedIcon(name="gtk-zoom-fit")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_quick.add(image)
        self.btn_quick.connect("clicked", self.on_btn_quick)
        self.hb.pack_end(self.btn_quick)


        # Label prob_infection de la enfermedad
        self.lbl_prob_inf = Gtk.Label(label="Probabilidad de infeccion:")
        self.grid.attach(self.lbl_prob_inf, 0, 0, 1, 1)

        # Entry prob_infection de la enfermedad
        self.entry_prob_infection = Gtk.Entry()
        self.entry_prob_infection.set_placeholder_text("Escriba aca...")
        self.entry_prob_infection.connect("changed", self.on_entry_active)
        self.grid.attach_next_to(self.entry_prob_infection, self.lbl_prob_inf, Gtk.PositionType.RIGHT, 1, 1)

        # Label pasos average de la enfermedad
        self.lbl_average_step = Gtk.Label(label="Cantidad de pasos promedio:")
        self.grid.attach_next_to(self.lbl_average_step, self.lbl_prob_inf, Gtk.PositionType.BOTTOM, 1, 1)

        # Entry pasos average
        self.entry_avg_step = Gtk.Entry()
        self.entry_avg_step.set_placeholder_text("Escriba aca...")
        self.entry_avg_step.connect("changed", self.on_entry_active)
        self.grid.attach_next_to(self.entry_avg_step, self.lbl_average_step, Gtk.PositionType.RIGHT, 1, 1)

        # Label poblacion de la comunidad
        self.lbl_population = Gtk.Label(label="Poblacion de la comunidad:")
        self.grid.attach_next_to(self.lbl_population, self.lbl_average_step, Gtk.PositionType.BOTTOM, 1, 1)

        # Entry poblacion de la comunidad
        self.entry_population = Gtk.Entry()
        self.entry_population.set_placeholder_text("Escriba aca...")
        self.entry_population.connect("changed", self.on_entry_active)
        self.grid.attach_next_to(self.entry_population, self.lbl_population, Gtk.PositionType.RIGHT, 1, 1)

        # Label average physical conection
        self.lbl_avg_phy_cnt = Gtk.Label(label="Promedio de contacto estrecho:")
        self.grid.attach_next_to(self.lbl_avg_phy_cnt, self.lbl_population, Gtk.PositionType.BOTTOM, 1, 1)

        # Entry average phyisical connection
        self.entry_avg_phy_cnt = Gtk.Entry()
        self.entry_avg_phy_cnt.set_placeholder_text("Escriba aca...")
        self.entry_avg_phy_cnt.connect("changed", self.on_entry_active)
        self.grid.attach_next_to(self.entry_avg_phy_cnt, self.lbl_avg_phy_cnt, Gtk.PositionType.RIGHT, 1, 1)

        # Label prob_physical conection
        self.lbl_pro_phy_cnt = Gtk.Label(label="Probabilidad de coneccion fisica:")
        self.grid.attach_next_to(self.lbl_pro_phy_cnt, self.lbl_avg_phy_cnt, Gtk.PositionType.BOTTOM, 1, 1)

        # Entry prob_physical connection
        self.entry_pro_phy_cnt = Gtk.Entry()
        self.entry_pro_phy_cnt.set_placeholder_text("Escriba aca...")
        self.entry_pro_phy_cnt.connect("changed", self.on_entry_active)
        self.grid.attach_next_to(self.entry_pro_phy_cnt, self.lbl_pro_phy_cnt, Gtk.PositionType.RIGHT, 1, 1)

        # Label cantidad de pasos de la simulacion
        self.lbl_steps = Gtk.Label(label="cantidad de pasos de la simulacion")
        self.grid.attach_next_to(self.lbl_steps, self.lbl_pro_phy_cnt, Gtk.PositionType.BOTTOM, 1, 1)

        # Entry cantidad de pasos de a simulacion
        self.entry_steps = Gtk.Entry()
        self.entry_steps.set_placeholder_text("Ingrese un numero entero...")
        self.entry_steps.connect("changed", self.on_entry_active)
        self.grid.attach_next_to(self.entry_steps, self.lbl_steps, Gtk.PositionType.RIGHT, 1, 1)

        # Grafico que representara las cosas
        self.graphic = Gtk.Image()
        self.grid.attach_next_to(self.graphic, self.entry_prob_infection, Gtk.PositionType.RIGHT, 3, 6)

        # Boton run simulacion
        self.btn_run = Gtk.Button()
        self.btn_run.set_label("Correr simulacion")
        self.btn_run.set_sensitive(False)
        self.grid.attach_next_to(self.btn_run, self.lbl_steps, Gtk.PositionType.BOTTOM, 2, 1)
        self.btn_run.connect("clicked", self.on_btn_run)

        # Boton de escoger ejes
        self.btn_select = Gtk.Button()
        self.btn_select.set_label("Escoger ejes para el grafico")
        self.btn_select.connect("clicked", self.on_btn_select)
        self.grid.attach_next_to(self.btn_select, self.btn_run, Gtk.PositionType.RIGHT, 1, 1)

        # Boton de reporte inicial
        self.btn_ini_report = Gtk.Button()
        self.btn_ini_report.set_label("Reporte inicial")
        self.btn_ini_report.connect("clicked", self.on_btn_report)
        self.grid.attach_next_to(self.btn_ini_report, self.btn_select, Gtk.PositionType.RIGHT, 1, 1)

        # Boton reporte final
        self.btn_final_report = Gtk.Button()
        self.btn_final_report.set_label("Reporte final")
        self.btn_final_report.connect("clicked", self.on_btn_report)
        self.grid.attach_next_to(self.btn_final_report, self.btn_ini_report, Gtk.PositionType.RIGHT, 1, 1)

        self.checks = {self.entry_prob_infection:0, self.entry_avg_step:0, self.entry_population:0,
                        self.entry_avg_phy_cnt:0, self.entry_pro_phy_cnt:0, self.entry_steps:0}

    def on_entry_active(self, entry):
        # chequear que el numero ingresado a la entry
        text = entry.get_text()
        if entry is self.entry_pro_phy_cnt or entry is self.entry_prob_infection:
            if is_number(text):
                if 0 <= float(text) <= 1:
                    self.checks[entry] = 1
                else:
                    self.checks[entry] = 0
            else:
                self.checks[entry] = 0
        else:
            if text.isnumeric():
                self.checks[entry] = 1
            else:
                self.checks[entry] = 0

        total = 0
        for num in self.checks.values():
            total += num
        if total == 6:
            self.btn_run.set_sensitive(True)
        else:
            self.btn_run.set_sensitive(False)

    def on_btn_run(self, btn):
        # Se crea enfermedad, comunidad y se ejecuta la simulacion
        self.prob_infection = float(self.entry_prob_infection.get_text())
        self.average_steps = int(self.entry_avg_step.get_text())
        self.population = int(self.entry_population.get_text())
        self.avg_phy_cnt = int(self.entry_avg_phy_cnt.get_text())
        self.prob_phy_cnt = float(self.entry_pro_phy_cnt.get_text())
        self.steps = int(self.entry_steps.get_text())

        self.disease = Disease(self.prob_infection, self.average_steps)
        self.community = Community(self.population, self.avg_phy_cnt, self.disease, self.prob_phy_cnt)
        self.sim = Simulator(self.community)
        self.data = self.sim.run(self.steps)
        self.data["report"]["steps"] = [num for num in range(0, self.steps)]

        on_info_clicked(self, "Informacion", "Se ha realizado la simulacion")

        self.btn_ini_report.show()
        self.btn_final_report.show()
        self.btn_select.show()

    def on_btn_report(self, btn):
        if btn is self.btn_ini_report:
            main = "Reporte Inicial"
            key = "init_report"
        else:
            main = "Reporte final"
            key = "final_report"

        second = ""
        for item in self.data[key].items():
            second += f"{item[0]}: {item[1]}" + "\n"
        on_info_clicked(self, main, second)

    def on_btn_select(self, btn):
        self.dialog = DialogSelect(self, self.data["report"])
        response = self.dialog.run()
        if response == Gtk.ResponseType.OK:
            # hacer las cosas del ploteo aca (?)
            self.x_axis = self.data["report"][self.dialog.x_axis.get_active_text()]
            self.y_axis = self.data["report"][self.dialog.y_axis.get_active_text()]
            plt.plot(self.x_axis, self.y_axis)
            plt.title(f"{self.dialog.y_axis.get_active_text()} vs {self.dialog.x_axis.get_active_text()}")
            plt.ylabel(self.dialog.y_axis.get_active_text())
            plt.xlabel(self.dialog.x_axis.get_active_text())
            plt.show()

        self.dialog.destroy()

    def on_btn_quick(self, btn):
        # Agrega datos listos a las entradas para que sea mas rapidos
        self.entry_prob_infection.set_text("0.7")
        self.entry_avg_step.set_text("14")
        self.entry_population.set_text("1000")
        self.entry_avg_phy_cnt.set_text("7")
        self.entry_pro_phy_cnt.set_text("0.5")
        self.entry_steps.set_text("150")


    def on_btn_about(self, btn):
        AboutDialog()

if __name__ == "__main__":
    window = MainWindow()
    window.show_all()
    window.graphic.hide()
    window.btn_ini_report.hide()
    window.btn_final_report.hide()
    window.btn_select.hide()
    Gtk.main()
