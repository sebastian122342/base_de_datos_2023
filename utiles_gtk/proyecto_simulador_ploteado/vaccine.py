# Vaccine

class Vaccine():
    """ Clase dedicada al objeto vacuna """
    def __init__(self, immunity, doses, steps, vax_id):
        # Se establece la vacuna
        self.immunity = immunity
        self.num_doses = doses
        self.steps = steps
        self.vax_id = vax_id


    @classmethod
    # Vacuna 1
    def new_vaccine1(cls):
        return cls(0.25, 2, 3, 1)

    @classmethod
    # Vacuna 2
    def new_vaccine2(cls):
        return cls(0.5, 2, 6, 2)

    @classmethod
    # Vacuna 3
    def new_vaccine3(cls):
        return cls(1, 1, 0, 3)

    @classmethod
    # No hay vacuna
    def no_vaccine(cls):
        return cls(0, 0, 0, 0)
