# dialogo de agregar informacion

# Se importan los modulos necesarios
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class DialogAdd(Gtk.Dialog):
    """ Ventana de dialogo para poder agregar informacion """
    def __init__(self):
        super().__init__(title="Agregar informacion")
        self.set_modal(True)
        self.add_buttons(Gtk.STOCK_OK, Gtk.ResponseType.OK,
                        Gtk.STOCK_CLOSE, Gtk.ResponseType.CLOSE)
        self.set_default_size(600, 600)

        self.box = self.get_content_area()
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.box.pack_start(self.grid, True, True, 1)

        self.lbl_id = Gtk.Label(label="ID:")
        self.grid.attach(self.lbl_id, 0, 0, 1, 1)

        self.entry_id = Gtk.Entry()
        self.entry_id.set_placeholder_text("Escribir ID")
        self.grid.attach(self.entry_id, 1, 0, 1, 1)

        self.lbl_name = Gtk.Label(label="Nombre:")
        self.grid.attach(self.lbl_name, 0, 1, 1, 1)

        self.entry_name = Gtk.Entry()
        self.entry_name.set_placeholder_text("Escribir nombre")
        self.grid.attach(self.entry_name, 1, 1, 1, 1)

        self.lbl_rating = Gtk.Label(label="Rating:")
        self.grid.attach(self.lbl_rating, 0, 2, 1, 1)

        self.combo_rating = Gtk.ComboBoxText()
        for x in range(1, 11):
            self.combo_rating.append_text(str(x))
        self.combo_rating.set_active(0)
        self.grid.attach(self.combo_rating, 1, 2, 1, 1)

        # 2 representa wrap mode "word" solo se cortan palabras
        self.wrap = Gtk.WrapMode(2)

        #Scrolled window para el texto
        self.scroll = Gtk.ScrolledWindow()
        self.grid.attach(self.scroll, 0, 3, 4, 4)

        # Texview
        self.textview = Gtk.TextView()
        self.textview.set_wrap_mode(self.wrap)
        self.buffer = self.textview.get_buffer()
        self.buffer.set_text("Escriba aqui la reseña")
        self.scroll.add(self.textview)






        self.show_all()
