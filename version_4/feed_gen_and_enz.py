# alimentar enzimas y genes a la bdd

import psycopg2
import xml.etree.ElementTree as et
from Bio import SeqIO
from io import StringIO

def main():

    print("excelentes tardes")
    # parsear el xml
    path = "/home/seba/Downloads/datos_bdd/hmdb_proteins.xml"
    tree = et.parse(path)
    root = tree.getroot()

    # conexion a la bdd
    conn = psycopg2.connect(database = "base_final", user="postgres", password="122342", host="localhost", port="5432")
    cursor = conn.cursor()

    print(root)
   
    # todas las proteinas del xml
    for prot in root.findall("{http://www.hmdb.ca}protein"):

        # sacar los datos
        ptype = prot.find("{http://www.hmdb.ca}protein_type").text
        if ptype == "Enzyme":
            #print(ptype)
            prot_prop = prot.find("{http://www.hmdb.ca}protein_properties")
            prot_seq = prot_prop.find("{http://www.hmdb.ca}polypeptide_sequence").text
            # comprobar que no sea none
            if prot_seq:
                fasta_file = StringIO(prot_seq)
                for record in SeqIO.parse(fasta_file, "fasta"):
                    enz_name = record.description
                    prot_seq = str(record.seq)
                
            gen_name = prot.find("{http://www.hmdb.ca}gene_name").text
            gen_prop = prot.find("{http://www.hmdb.ca}gene_properties")
            gen_seq = gen_prop.find("{http://www.hmdb.ca}gene_sequence").text
            if gen_seq:
                fasta_file = StringIO(gen_seq)
                for record in SeqIO.parse(fasta_file, "fasta"):
                    gen_seq = str(record.seq)
            # agregar cromosoma y locus al gen tambien
            chr_loc = gen_prop.find("{http://www.hmdb.ca}chromosome_location").text
            locus = gen_prop.find("{http://www.hmdb.ca}locus").text

            #print(f"Gen-> secuencia {gen_seq}\tnombre {gen_name}")
            #print(f"Enzima-> secuencia {prot_seq}\tnombre {enz_name}, sacar id del gen")

            # preparar la insercion, primero gen
            insert_query = "INSERT INTO gen (nombre, seq, cromosoma, locus) VALUES (%s, %s, %s, %s)"
            data = (gen_name, gen_seq, chr_loc, locus)

            # insertar y commitear, try para evitar repeticiones de nombre
            try:
                cursor.execute(insert_query, data)
                conn.commit()
            except:
                conn.rollback()

            # Segunda insercion, en tabla enzima
            cursor.execute("SELECT id FROM gen WHERE nombre = %s", (gen_name,))
            idgen = cursor.fetchone()

            insert_query = "INSERT INTO enzima (nombre, seq, idgen) VALUES (%s, %s, %s)"
            data = (enz_name, prot_seq, idgen[0] if idgen is not None else None)

            # insertar y commitear, try para evitar repeticiones de nombre
            try:
                cursor.execute(insert_query, data)
                conn.commit()
            except:
                conn.rollback()
                continue # si ya se encuentra la enzima entonces no se vuelven a hacer los otros enlaces con metabolitos y rutas
            
            # Sacar el id de la enzima, para conexiones futuras
            cursor.execute("SELECT id FROM enzima WHERE nombre = %s", (enz_name,))
            idenz = cursor.fetchone()

            # Hacer la conexion de enzima - metabolito
            mets_connected = prot.find("{http://www.hmdb.ca}metabolite_associations")
            for met in mets_connected:
                met_name = met.find("{http://www.hmdb.ca}name").text
                
                # obtener el id del metabolito a conectar, si no esta el metabolito entonces no se hace coneccion
                cursor.execute("SELECT id FROM metabolito WHERE nombre = %s", (met_name,))
                idmet = cursor.fetchone()

                if idmet:
                    # Comando de insercion 
                    insert_query = "INSERT INTO enz_metabolito (idenz, idmet) VALUES (%s, %s)"
                    data = (idenz, idmet) # Si el metabolito de la relacion no esta en el archivo de metabolitos, entonces esa enzima no tiene uso en el contexto evaluado

                     # insertar y commitear
                    cursor.execute(insert_query, data)
                    conn.commit()
            
            # Hacer la conexion de enzima - ruta
            paths_connected = prot.find("{http://www.hmdb.ca}pathways")
            for pathw in paths_connected:
                # sacar el nombre de la ruta especifica
                path_name = pathw.find("{http://www.hmdb.ca}name").text
                path_names = path_name.split(" ")
                if len(path_names) > 1:
                    pathway = path_names[0] + " " + path_name[1]
                else:
                    pathway = path_names[0]

                # seleccionar el id de la ruta
                cursor.execute("SELECT id FROM ruta WHERE nombre = %s", (pathway,))
                idruta = cursor.fetchone()
                # Solamente se añade registro si es que la ruta se asocia con la enzima, si no no
                if idruta:
                    # Comando de insercion
                    insert_query = "INSERT INTO ruta_enzima (idenz, idruta) VALUES (%s, %s)"
                    data = (idenz, idruta)

                    # Insertar y commitear
                    cursor.execute(insert_query, data)
                    conn.commit()

                

                
if __name__ == "__main__":
    main()
