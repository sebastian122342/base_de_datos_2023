# Ventana principal del proyecto de base de datos - ultra preliminar

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf

from about_dialog import AboutDialog

from messages import on_error_clicked, on_info_clicked, on_question_clicked

import psycopg2

from rdkit import Chem
from rdkit.Chem import Draw

conn = psycopg2.connect(database="base_final", user="postgres", password="122342", host="localhost", port="5432")

cursor = conn.cursor()

class MainWindow(Gtk.Window):
    def __init__(self):
        super().__init__()
        #self.set_default_size(1200, 900)
        self.maximize()
        self.set_border_width(20)
        self.connect("destroy", Gtk.main_quit)

        # Se crea la grid que contendrá los demas widgets
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        # Se crea la headerbar
        self.hb = Gtk.HeaderBar()
        self.hb.set_title("Base de datos de metabolomica")
        self.hb.set_subtitle("Interfaz gráfica")
        self.hb.set_show_close_button(True)
        self.set_titlebar(self.hb)

        # Boton Ventana About
        self.btn_about = Gtk.Button()
        icon = Gio.ThemedIcon(name="help-about")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_about.add(image)
        self.btn_about.connect("clicked", self.on_btn_about)
        self.hb.pack_end(self.btn_about)

        # Scrolledwindow para el Treeview
        self.scrolledw = Gtk.ScrolledWindow()
        self.grid.attach(self.scrolledw, 0, 0, 20, 30)

        # Treeview
        self.tree = Gtk.TreeView();
        self.tree.connect("row_activated", self.on_tree_row_activated)
        self.tree.set_activate_on_single_click(True)
        self.tree.set_enable_search(True)
        self.scrolledw.add(self.tree)

        # ComboboxText para las tablas
        self.combo_tables = Gtk.ComboBoxText()
        table_names = ["metabolito", "enzima", "gen", "ruta"]
        for name in table_names:
            self.combo_tables.append_text(name)
        self.combo_tables.connect("changed", self.on_combo_changed)
        self.hb.pack_start(self.combo_tables)

        # Imagen para los metabolitos
        self.met_img = Gtk.Image()
        self.grid.attach_next_to(self.met_img, self.scrolledw, Gtk.PositionType.RIGHT, 1, 1)

        # Label para las secuencias de genes y enzimas
        self.scrolledw_seq = Gtk.ScrolledWindow()
        self.lbl_seq = Gtk.Label()
        self.lbl_seq.props.wrap = True
        self.lbl_seq.set_line_wrap_mode(1)
        self.lbl_seq.set_xalign(0)
        self.lbl_seq.set_yalign(0)
        self.lbl_seq.set_use_markup(True)
        self.lbl_seq.set_selectable(True)
        self.scrolledw_seq.add(self.lbl_seq)
        self.grid.attach_next_to(self.scrolledw_seq, self.scrolledw, Gtk.PositionType.RIGHT, 20, 30)

        # Test de la carga de moleculas
        #m = Chem.MolFromSmiles('Cc1ccccc1')
        #img = Draw.MolToFile(m, "mol.png")
        #self.met_img.set_from_file("mol.png")
        
        # Boton para añadir registros
        self.btn_add = Gtk.Button()
        self.btn_add_box = Gtk.Box()
        icon = Gio.ThemedIcon(name="gtk-add")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_add_box.add(image)
        self.lbl_btn_add = Gtk.Label(label="ADD")
        self.btn_add_box.add(self.lbl_btn_add)
        self.btn_add_box.set_spacing(5)
        self.btn_add.add(self.btn_add_box)
        self.grid.attach_next_to(self.btn_add, self.scrolledw, Gtk.PositionType.BOTTOM, 1, 1)
        #self.grid.attach(self.btn_add, 0, 2, 1, 1)
        self.btn_add.connect("clicked", self.on_btn_add)
        
        # Boton para editar registros
        self.btn_edit = Gtk.Button()
        self.btn_edit_box = Gtk.Box()
        icon = Gio.ThemedIcon(name="gtk-edit")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_edit_box.add(image)
        self.lbl_btn_edit = Gtk.Label(label="EDIT")
        self.btn_edit_box.add(self.lbl_btn_edit)
        self.btn_edit_box.set_spacing(5)
        self.btn_edit.add(self.btn_edit_box)
        self.grid.attach_next_to(self.btn_edit, self.btn_add, Gtk.PositionType.RIGHT, 1, 1)
        self.btn_edit.connect("clicked", self.on_btn_edit)
        
        
        # Boton para eliminar registros
        self.btn_del = Gtk.Button()
        self.btn_del_box = Gtk.Box()
        icon = Gio.ThemedIcon(name="gtk-delete")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_del_box.add(image)
        self.lbl_btn_del = Gtk.Label(label="DELETE")
        self.btn_del_box.add(self.lbl_btn_del)
        self.btn_del_box.set_spacing(5)
        self.btn_del.add(self.btn_del_box)
        self.grid.attach_next_to(self.btn_del, self.btn_edit, Gtk.PositionType.RIGHT, 1, 1)
        self.btn_del.connect("clicked", self.on_btn_del)


    def make_treeview(self, records, columns):

        if self.tree.get_columns():
            for column in self.tree.get_columns():
                self.tree.remove_column(column)

        column_length = len(columns)
        tmodel = Gtk.ListStore(*(column_length*[str]))
        self.tree.set_model(model=tmodel)

        cell = Gtk.CellRendererText()

        for item in range(len(columns)):
            column = Gtk.TreeViewColumn(columns[item][0], cell, text=item)
            self.tree.append_column(column)
            column.set_sort_column_id(item)
        for item in records:
            line = [str(x) for x in item]
            tmodel.append(line)

    def get_table_data(self, table_name):
        cursor.execute(f"SELECT column_name FROM information_schema.columns WHERE table_name = '{table_name}' order by ordinal_position;")
        columns = cursor.fetchall()

        cursor.execute(f"SELECT * FROM {table_name};")
        records = cursor.fetchall()

        return records, columns

    def on_tree_row_activated(self, tree, path, col):
        # ir cambiando la imagen del metabolito que se este viendo
        #print(col.props.title)
        #print(path)
        table = self.combo_tables.get_active_text()
        model = tree.get_model()
        
        if table == "metabolito":
        
            smile = model[path][2] # irse a la fila numero path, columna de indice 2
            m = Chem.MolFromSmiles(smile)
            Draw.MolToFile(m, "mol.png")
            self.met_img.set_from_file("mol.png")
            self.met_img.show()
        elif table == "gen" or table == "enzima":
            seq = model[path][2]
            self.lbl_seq.set_label(f"<b>Sequence:</b>\n{seq}")
            self.scrolledw_seq.show()

    def on_combo_changed(self, combo):

        table = combo.get_active_text()
        records, columns = self.get_table_data(table)
        self.make_treeview(records, columns)

        if table == "metabolito" or table == "enzima" or table == "gen":
            col = self.tree.get_column(2)
            col.set_visible(False)
        if table != "metabolito":
            self.met_img.hide()
        if table != "gen" or table != "enzima":
            self.scrolledw_seq.hide()
            
        self.btn_add.show()
        self.btn_edit.show()
        self.btn_del.show()
        self.scrolledw.show()

    def on_btn_add(self, btn):
        print("hola añadir un registro")

    def on_btn_edit(self, btn):
        print("hola editar un registro")

    def on_btn_del(self, btn):
        print("hola eliminar un registro")

    def on_btn_about(self, btn):
        AboutDialog()

if __name__ == "__main__":
    window = MainWindow()
    window.show_all()
    window.btn_add.hide()
    window.btn_edit.hide()
    window.btn_del.hide()
    window.scrolledw.hide()
    window.met_img.hide()
    window.scrolledw_seq.hide()
    Gtk.main()

