# About dialog

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import urllib.request

url = "https://gitlab.com/sebastian122342/base_de_datos_2023.git"

class AboutDialog(Gtk.AboutDialog):
    def __init__(self):
        super().__init__(title="Dialogo About")
        self.set_modal(True)
        autores = ["Sebastián Bustamante", "Magdalena Lolas", "Cecilia Castillo"]
        self.add_credit_section("Autores", autores)
        self.set_comments("Base de datos 2023")
        self.set_logo_icon_name("input-gaming")
        self.set_program_name("Base de datos de metabolómica")
        self.set_version("1.0")
        self.set_website(url)
        self.set_website_label("Repositorio de este proyecto")
        self.connect("activate-link", self.abrir_link)

        self.show_all()

    def abrir_link(self, cosita, nose):
        urllib.request.urlopen(self.get_website())
