# Ventana principal del proyecto de base de datos - ultra preliminar

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf

from about_dialog import AboutDialog

from messages import on_error_clicked, on_info_clicked, on_question_clicked

import psycopg2

from rdkit import Chem
from rdkit.Chem import Draw

from cairosvg import svg2png

conn = psycopg2.connect(database="bdd_final", user="postgres", password="122342", host="localhost", port="5432")

cursor = conn.cursor()

class MainWindow(Gtk.Window):
    def __init__(self):
        super().__init__()
        #self.set_default_size(1200, 900)
        self.maximize()
        self.set_border_width(20)
        self.connect("destroy", Gtk.main_quit)

        # Se crea la grid que contendrá los demas widgets
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        # Se crea la headerbar
        self.hb = Gtk.HeaderBar()
        self.hb.set_title("Base de datos de metabolomica")
        self.hb.set_subtitle("Interfaz gráfica")
        self.hb.set_show_close_button(True)
        self.set_titlebar(self.hb)

        # Boton Ventana About
        self.btn_about = Gtk.Button()
        icon = Gio.ThemedIcon(name="help-about")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_about.add(image)
        self.btn_about.connect("clicked", self.on_btn_about)
        self.hb.pack_end(self.btn_about)

        # Scrolledwindow para el Treeview
        self.scrolledw = Gtk.ScrolledWindow()
        self.scrolledw.set_max_content_height(20)
        #self.scrolledw.set_propagate_natural_height(True)
        self.grid.attach(self.scrolledw, 0, 0, 10, 30)

        # Treeview
        self.tree = Gtk.TreeView();
        self.tree.connect("row_activated", self.on_tree_row_activated)
        self.tree.set_activate_on_single_click(True)
        self.tree.set_enable_search(True)
        self.scrolledw.add(self.tree)

        # ComboboxText para las tablas
        self.combo_tables = Gtk.ComboBoxText()
        table_names = ["metabolito", "enzima", "gen", "ruta"]
        for name in table_names:
            self.combo_tables.append_text(name)
        self.combo_tables.connect("changed", self.on_combo_changed)
        self.hb.pack_start(self.combo_tables)

        # Widgets particulares para los metabolitos
        
        # Imagen para los metabolitos
        self.met_img = Gtk.Image()
        self.grid.attach_next_to(self.met_img, self.scrolledw, Gtk.PositionType.RIGHT, 1, 1)

        # Label para id
        self.lbl_id = Gtk.Label(label="ID")
        self.grid.attach_next_to(self.lbl_id, self.met_img, Gtk.PositionType.BOTTOM, 1, 1)

        # Entrada para el id del metabolito - inmodificable
        self.entry_id = Gtk.Entry()
        self.entry_id.set_editable(False)
        self.grid.attach_next_to(self.entry_id, self.lbl_id, Gtk.PositionType.RIGHT, 1, 1)
        
        # Label1 para lo que se necesite
        self.lbl1 = Gtk.Label(label="Label 1")
        self.grid.attach_next_to(self.lbl1, self.lbl_id, Gtk.PositionType.BOTTOM, 1, 1)

        # Entrada 1
        self.entry1 = Gtk.Entry()
        self.entry1.connect("changed", self.on_widg_changed)
        self.grid.attach_next_to(self.entry1, self.lbl1, Gtk.PositionType.RIGHT, 1, 1)

        # Label2
        self.lbl2 = Gtk.Label(label="Label 2")
        self.grid.attach_next_to(self.lbl2, self.lbl1, Gtk.PositionType.BOTTOM, 1, 1)

        # Entrada2
        self.entry2 = Gtk.Entry()
        self.entry2.connect("changed", self.on_widg_changed)
        self.grid.attach_next_to(self.entry2, self.lbl2, Gtk.PositionType.RIGHT, 1, 1)

        # Label3
        self.lbl3 = Gtk.Label(label="Label 3")
        self.grid.attach_next_to(self.lbl3, self.lbl2, Gtk.PositionType.BOTTOM, 1, 1)

        # Entrada3
        self.entry3 = Gtk.Entry()
        self.entry3.connect("changed", self.on_widg_changed)
        self.grid.attach_next_to(self.entry3, self.lbl3, Gtk.PositionType.RIGHT, 1, 1)
        
        # Label4 para el mw
        self.lbl4 = Gtk.Label(label="Label 4")
        self.grid.attach_next_to(self.lbl4, self.lbl3, Gtk.PositionType.BOTTOM, 1, 1)

        # SpinButton para numero
        self.sp = Gtk.SpinButton()
        self.sp.set_range(0, 1000)
        #self.sp.set_climb_rate(2)
        self.sp.configure(None, 1.00, 3)
        self.sp.set_numeric(False)
        self.sp.set_numeric(True)
        self.sp.set_increments(1, 5)
        self.sp.connect("changed", self.on_widg_changed)
        self.grid.attach_next_to(self.sp, self.lbl4, Gtk.PositionType.RIGHT, 1, 1)

        # Entry para las secuencias de genes y enzimas
        self.seq_text = Gtk.TextView()
        self.buffer_seq = self.seq_text.get_buffer()
        self.buffer_seq.connect("changed", self.on_widg_changed)
        self.seq_text.set_wrap_mode(Gtk.WrapMode.CHAR)
        # Label para las secuencias de genes y enzimas
        self.scrolledw_seq = Gtk.ScrolledWindow()
        self.scrolledw_seq.set_max_content_height(20)
        self.grid.attach_next_to(self.scrolledw_seq, self.lbl4, Gtk.PositionType.BOTTOM, 10, 5)
        self.lbl_seq = Gtk.Label(label = "Secuencia")
        self.grid.attach_next_to(self.lbl_seq, self.scrolledw_seq, Gtk.PositionType.TOP, 1, 1)
        self.scrolledw_seq.add(self.seq_text)
        #self.grid.attach_next_to(self.scrolledw_seq, self.lbl4, Gtk.PositionType.BOTTOM, 5, 5)

        # Test de la carga de moleculas
        #m = Chem.MolFromSmiles('Cc1ccccc1')
        #img = Draw.MolToFile(m, "mol.png")
        #self.met_img.set_from_file("mol.png")

        # Scrolled window para el tema de la tabla intermedia 1
        self.scrolledw_int1 = Gtk.ScrolledWindow()
        self.tree_int1 = Gtk.TreeView()
        #self.tree_int1.connect("row_activated", self.on_tree_int1_row_activated)
        self.tree_int1.set_activate_on_single_click(True)
        self.tree_int1.set_enable_search(True)
        self.scrolledw_int1.add(self.tree_int1)
        self.grid.attach_next_to(self.scrolledw_int1, self.scrolledw_seq, Gtk.PositionType.BOTTOM, 5, 10)

        self.scrolledw_int2 = Gtk.ScrolledWindow()
        self.tree_int2 = Gtk.TreeView()
        self.tree_int2.set_activate_on_single_click(True)
        self.tree_int2.set_enable_search(True)
        self.scrolledw_int2.add(self.tree_int2)
        self.grid.attach_next_to(self.scrolledw_int2, self.scrolledw_int1, Gtk.PositionType.RIGHT, 20, 10)

        self.celltoggle1 = Gtk.CellRendererToggle()
        self.celltoggle1.set_activatable(True)
        self.celltoggle1.connect("toggled", self.on_celltoggle1_clicked)

        self.celltoggle2 = Gtk.CellRendererToggle()
        self.celltoggle2.set_activatable(True)
        self.celltoggle2.connect("toggled", self.on_celltoggle2_clicked)
        
        # Boton para añadir registros
        self.btn_add = Gtk.Button()
        self.btn_add_box = Gtk.Box()
        icon = Gio.ThemedIcon(name="gtk-add")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_add_box.add(image)
        self.lbl_btn_add = Gtk.Label(label="ADD")
        self.btn_add_box.add(self.lbl_btn_add)
        self.btn_add_box.set_spacing(5)
        self.btn_add.add(self.btn_add_box)
        self.grid.attach_next_to(self.btn_add, self.scrolledw, Gtk.PositionType.BOTTOM, 1, 1)
        #self.grid.attach(self.btn_add, 0, 2, 1, 1)
        self.btn_add.connect("clicked", self.on_btn_add)
        
        # Boton para editar registros
        self.btn_edit = Gtk.Button()
        self.btn_edit_box = Gtk.Box()
        icon = Gio.ThemedIcon(name="gtk-edit")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_edit_box.add(image)
        self.lbl_btn_edit = Gtk.Label(label="EDIT")
        self.btn_edit_box.add(self.lbl_btn_edit)
        self.btn_edit_box.set_spacing(5)
        self.btn_edit.add(self.btn_edit_box)
        self.grid.attach_next_to(self.btn_edit, self.btn_add, Gtk.PositionType.RIGHT, 1, 1)
        self.btn_edit.connect("clicked", self.on_btn_edit)
        self.btn_edit.set_sensitive(False)
        
        # Boton para eliminar registros
        self.btn_del = Gtk.Button()
        self.btn_del_box = Gtk.Box()
        icon = Gio.ThemedIcon(name="gtk-delete")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_del_box.add(image)
        self.lbl_btn_del = Gtk.Label(label="DELETE")
        self.btn_del_box.add(self.lbl_btn_del)
        self.btn_del_box.set_spacing(5)
        self.btn_del.add(self.btn_del_box)
        self.grid.attach_next_to(self.btn_del, self.btn_edit, Gtk.PositionType.RIGHT, 1, 1)
        self.btn_del.connect("clicked", self.on_btn_del)


    def on_widg_changed(self, widg):
        self.btn_del.set_sensitive(False)
        self.btn_edit.set_sensitive(True)
        self.btn_add.set_sensitive(True)
        
    def make_treeview(self, records, columns, tree):

        if tree.get_columns():
            for column in tree.get_columns():
                tree.remove_column(column)

        column_length = len(columns)
        tmodel = Gtk.ListStore(*(column_length*[str]))
        tree.set_model(model=tmodel)

        cell = Gtk.CellRendererText()

        for item in range(len(columns)):
            column = Gtk.TreeViewColumn(columns[item][0], cell, text=item)
            tree.append_column(column)
            column.set_sort_column_id(item)
        for item in records:
            line = [str(x) for x in item]
            tmodel.append(line)

    def make_int1_treeview(self, records, columns, tree):
        if tree.get_columns():
            for column in tree.get_columns():
                tree.remove_column(column)

        column_length = len(columns)
        tmodel = Gtk.ListStore(str, bool)
        tree.set_model(model=tmodel)

        celltext = Gtk.CellRendererText()
        
        column_text = Gtk.TreeViewColumn(columns[0], celltext, text=0)
        column_text.set_sort_column_id(0)
        
        column_toggle = Gtk.TreeViewColumn(columns[1], self.celltoggle1, active=1)
        column_toggle.set_sort_column_id(1)
        
        tree.append_column(column_text)
        tree.append_column(column_toggle)

        for item in records:
            text = str(item[0])
            checked = int(item[1])
            tmodel.append([text, checked])

    def make_int2_treeview(self, records, columns, tree):
        if tree.get_columns():
            for column in tree.get_columns():
                tree.remove_column(column)

        column_length = len(columns)
        tmodel = Gtk.ListStore(str, bool)
        tree.set_model(model=tmodel)

        celltext = Gtk.CellRendererText()
        
        column_text = Gtk.TreeViewColumn(columns[0], celltext, text=0)
        column_text.set_sort_column_id(0)

        
        column_toggle = Gtk.TreeViewColumn(columns[1], self.celltoggle2, active=1)
        column_toggle.set_sort_column_id(1)
        
        tree.append_column(column_text)
        tree.append_column(column_toggle)

        for item in records:
            text = str(item[0])
            checked = int(item[1])
            tmodel.append([text, checked])

    def on_celltoggle1_clicked(self, toggle, path):
        past = toggle.get_active()
        model = self.tree_int1.get_model()
        model[path][1] = not past
        self.tree_int1.set_model(model)
        self.btn_add.set_sensitive(True)
        self.btn_edit.set_sensitive(True)

    def on_celltoggle2_clicked(self, toggle, path):
        past = toggle.get_active()
        model = self.tree_int2.get_model()
        model[path][1] = not past
        self.tree_int2.set_model(model)
        self.btn_add.set_sensitive(True)
        self.btn_edit.set_sensitive(True)

    def get_table_data(self, table_name):
        cursor.execute(f"SELECT column_name FROM information_schema.columns WHERE table_name = '{table_name}' order by ordinal_position;")
        columns = cursor.fetchall()

        cursor.execute(f"SELECT * FROM {table_name};")
        records = cursor.fetchall()

        return records, columns

    def on_tree_row_activated(self, tree, path, col):
        # ir cambiando la imagen del metabolito que se este viendo
        #print(col.props.title)
        #print(path)
        table = self.combo_tables.get_active_text()
        model = tree.get_model()
        
        self.btn_add.show()
        self.btn_edit.show()
        self.btn_del.show()
        
        if table == "metabolito":
        
            smile = model[path][2] # irse a la fila numero path, columna de indice 2
            m = Chem.MolFromSmiles(smile)
            Draw.MolToFile(m, "mol.png")
            self.met_img.set_from_file("mol.png")
            #self.met_img.show()

            # Los particulares
            self.entry_id.set_text(model[path][0])

            self.lbl1.set_label("Nombre")
            self.entry1.set_text(model[path][1])
            
            self.lbl2.set_label("Smiles")
            self.entry2.set_text(model[path][2])
            
            self.lbl3.set_label("Formula")
            self.entry3.set_text(model[path][3])

            self.lbl4.set_label("Peso molecular")
            self.sp.set_value(float(model[path][4]))

            met_id = model[path][0]            
            
            # relaciones de enzima con metabolito
            # se tiene una tabla con todas las enzimas, y un checkbox en caso
            # de que el metabolito este unido con esa enzima
            
            cursor.execute("SELECT nombre FROM enzima");
            enz_names = cursor.fetchall()
            search_query = f"SELECT enzima.nombre as nombre_enz FROM enz_metabolito INNER JOIN metabolito ON enz_metabolito.idmet = metabolito.id INNER JOIN enzima ON enz_metabolito.idenz = enzima.id WHERE enz_metabolito.idmet = {met_id}";
            cursor.execute(search_query)
            enz_con = cursor.fetchall()
            final_regist = self.get_treeint_data(enz_con, enz_names)
            self.make_int1_treeview(final_regist, ["Enzima","Relacionado"], self.tree_int1)

            # relaciones de metabolito con ruta
            cursor.execute("SELECT nombre FROM ruta");
            pw_names = cursor.fetchall()
            search_query = f"SELECT ruta.nombre as nombre_ruta FROM ruta_metabolito INNER JOIN metabolito ON ruta_metabolito.idmet = metabolito.id INNER JOIN ruta ON ruta_metabolito.idruta = ruta.id WHERE ruta_metabolito.idmet = {met_id}";
            cursor.execute(search_query)
            pw_con = cursor.fetchall()
            final_pws = self.get_treeint_data(pw_con, pw_names)

            self.make_int2_treeview(final_pws, ["Ruta", "Relacionado"], self.tree_int2)
            
            self.show_met()
            
        elif table == "gen":

             # Los particulares
            self.entry_id.set_text(model[path][0])

            self.lbl1.set_label("Nombre")
            self.entry1.set_text(model[path][1])
            
            self.lbl2.set_label("Cromosoma")
            self.entry2.set_text(model[path][3])
            
            self.lbl3.set_label("Locus")
            self.entry3.set_text(model[path][4])

            self.lbl_seq.set_label("Secuencia")
            #buffer = self.seq_text.get_buffer()
            self.buffer_seq.set_text(model[path][2])
            #self.scrolledw_seq.show()
            
            #self.entry_name.show()

            self.show_gene()
            
        elif table == "enzima":
            # particulares

            self.entry_id.set_text(model[path][0])

            self.lbl1.set_label("Nombre")
            self.entry1.set_text(model[path][1])
            
            self.lbl2.set_label("idgen")
            self.entry2.set_text(model[path][3])

            #self.lbl_seq.set_label(f"<b>Sequence:</b>n {model[path][2]}")
            #buffer = self.seq_text.get_buffer()
            self.lbl_seq.set_label("Secuencia")
            self.buffer_seq.set_text(model[path][2])
            
            idenz = model[path][0]

            # union con la tabla enz_metabolito
            cursor.execute("SELECT nombre FROM metabolito")
            met_names = cursor.fetchall()
            search_query = f"SELECT metabolito.nombre as nombre_met FROM enz_metabolito INNER JOIN metabolito ON enz_metabolito.idmet = metabolito.id INNER JOIN enzima ON enz_metabolito.idenz = enzima.id WHERE enz_metabolito.idenz = {idenz}";
            cursor.execute(search_query)
            met_con = cursor.fetchall()
            final_regist = self.get_treeint_data(met_con, met_names)
            self.make_int1_treeview(final_regist, ["Metabolito", "Relacionado"], self.tree_int1)
            
            # union con la tabla ruta_enzima
            cursor.execute("SELECT nombre FROM ruta")
            pw_names = cursor.fetchall()
            search_query = f"SELECT ruta.nombre as nombre_ruta FROM ruta_enzima INNER JOIN ruta ON ruta_enzima.idruta = ruta.id INNER JOIN enzima ON ruta_enzima.idenz = enzima.id WHERE ruta_enzima.idenz = {idenz}"
            cursor.execute(search_query)
            pw_con = cursor.fetchall()
            final_regist = self.get_treeint_data(pw_con, pw_names)
            self.make_int2_treeview(final_regist, ["Ruta", "Relacionado"], self.tree_int2)
            
            self.show_enz()
            
        elif table == "ruta":

            # foto de la ruta metabolica
            svg_data = model[path][3]

            try:
                svg2png(bytestring=svg_data, write_to="pathway.png")
                self.met_img.set_from_file("pathway.png")
            except:
                pass
            
            self.entry_id.set_text(model[path][0])

            self.lbl1.set_label("Nombre")
            self.entry1.set_text(model[path][1])

            self.lbl2.set_label("ID SMPDB")
            self.entry2.set_text(model[path][2])

            self.lbl_seq.set_label("Data del svg")
            self.buffer_seq.set_text(model[path][3])
            
            
            idpw = model[path][0]

            # union con la tabla ruta_metabolito

            cursor.execute("SELECT nombre FROM metabolito")
            met_names = cursor.fetchall()
            search_query = f"SELECT metabolito.nombre as nombre_met FROM ruta_metabolito INNER JOIN metabolito ON ruta_metabolito.idmet = metabolito.id INNER JOIN ruta ON ruta_metabolito.idruta = ruta.id WHERE ruta_metabolito.idruta = {idpw}"
            cursor.execute(search_query)
            met_con = cursor.fetchall()
            final_regist = self.get_treeint_data(met_con, met_names)
            self.make_int1_treeview(final_regist, ["Metabolito", "Relacionado"], self.tree_int1)
            
            # union con la tabla ruta_enzima
            cursor.execute("SELECT nombre FROM enzima")
            enz_names = cursor.fetchall()
            search_query = f"SELECT enzima.nombre as nombre_enz FROM ruta_enzima INNER JOIN ruta ON ruta_enzima.idruta = ruta.id INNER JOIN enzima ON ruta_enzima.idenz = enzima.id WHERE ruta_enzima.idruta = {idpw}"
            cursor.execute(search_query)
            enz_con = cursor.fetchall()
            final_regist = self.get_treeint_data(enz_con, enz_names)
            self.make_int2_treeview(final_regist, ["Enzima","Relacionado"], self.tree_int2)
            
            self.show_pw()
        self.btn_del.set_sensitive(True)
        self.btn_edit.set_sensitive(False)
        self.btn_add.set_sensitive(False)

    def get_treeint_data(self, con, full):
        final = []
        for regist in full:
            name = regist[0]
            if con:
                #print(regist[0])
                if self.check_existence(con, name):
                    final.append((name, True))
                else:
                    final.append((name, False))
            else:
                final.append((name, False))
        return final

    def check_existence(self, con, name):
        for tupl in con:
            real_name = tupl[0]
            if real_name == name:
                return True
        return False
    
    def on_combo_changed(self, combo):

        table = combo.get_active_text()
        records, columns = self.get_table_data(table)
        self.make_treeview(records, columns, self.tree)
        self.hide_widgs()


        if table == "metabolito":
            col = self.tree.get_column(2)
            col.set_visible(False)
            col = self.tree.get_column(3)
            col.set_visible(False)
            col = self.tree.get_column(4)
            col.set_visible(False)
            
        elif table == "enzima":
            col = self.tree.get_column(2)
            col.set_visible(False)
            
        elif table == "gen":
            col = self.tree.get_column(2)
            col.set_visible(False)
            pass
        elif table == "ruta":
            col = self.tree.get_column(3)
            col.set_visible(False)

        #self.btn_add.show()
        #self.btn_edit.show()
        #self.btn_del.show()
        self.scrolledw.show()

    def show_pw(self):
        #self.lbl_id.show()
        #self.entry_id.show()

        self.lbl1.show()
        self.entry1.show()

        self.lbl2.show()
        self.entry2.show()

        self.scrolledw_int1.show()
        self.scrolledw_int2.show()

        self.lbl_seq.show()
        self.scrolledw_seq.show()
        
        #self.met_img.show()
        
    def show_enz(self):
        #self.lbl_id.show()
        #self.entry_id.show()

        self.lbl1.show()
        self.entry1.show()

        self.lbl2.show()
        self.entry2.show()

        self.scrolledw_int1.show()
        self.scrolledw_int2.show()
        
        self.scrolledw_seq.show()
        self.lbl_seq.show()
        
    def show_gene(self):
        #self.lbl_id.show()
        #self.entry_id.show()
        
        self.lbl1.show()
        self.entry1.show()

        self.lbl2.show()
        self.entry2.show()

        self.lbl3.show()
        self.entry3.show()

        self.lbl_seq.show()
        self.scrolledw_seq.show()

        #manejar el tema de la label de secuencia

    def show_met(self):

        #self.lbl_id.show()
        #self.entry_id.show()
        
        self.lbl1.show()
        self.entry1.show()
        
        self.lbl2.show()
        self.entry2.show()
        
        self.lbl3.show()
        self.entry3.show()
        
        self.lbl4.show()
        self.sp.show()

        #self.met_img.show()

        self.scrolledw_int1.show()
        self.scrolledw_int2.show()
        
    def hide_widgs(self):
        self.lbl1.hide()
        self.lbl2.hide()
        self.lbl3.hide()
        self.lbl4.hide()

        self.entry1.hide()
        self.entry2.hide()
        self.entry3.hide()
        self.sp.hide()

        self.lbl_id.hide()
        self.entry_id.hide()
        
        self.scrolledw_seq.hide()
        self.lbl_seq.hide()

        self.met_img.hide()

        self.scrolledw_int1.hide()
        self.scrolledw_int2.hide()
        
    def on_btn_add(self, btn):
        print("hola añadir un registro")
        table_name = self.combo_tables.get_active_text()
        model = self.tree.get_model()
        path = self.tree.get_cursor()[0]
        idobj = model[path][0]

        if table_name == "metabolito":
            #id, nombre, peso_mol, formula, smiles
            insert_query = "INSERT INTO metabolito (nombre, smiles, formula, peso_mol) VALUES (%s, %s, %s, %s);"
            name = self.entry1.get_text()
            sml = self.entry2.get_text()
            formula = self.entry3.get_text()
            mw = self.sp.get_value()
            data = (name, sml, formula, mw)

            try:
                
                cursor.execute(insert_query, data)
                conn.commit()
            except: # falla cuando ya se repite el nombre
                conn.rollback()
                on_error_clicked(self, principal="Error", secundaria="El nombre del metabolito debe ser unico")
                return

            # hacer las cosas de las tablas intermedias
            # 1 es enz_metabolito, 2 es ruta_metabolito
            model_int1 = self.tree_int1.get_model()
            for row in model_int1: #row[0] es el nombre row[1] es el bool
                if row[1]:
                    insert_query = "INSERT INTO enz_metabolito (idmet, idenz) VALUES (%s, %s)"
                    search_query = "SELECT id FROM enzima WHERE nombre = %s"
                    cursor.execute(search_query, (row[0],))
                    idenz = cursor.fetchone()
                    data = (idobj, idenz)
                    cursor.execute(insert_query, data)
                    conn.commit()
            # para la otra tabla intermedia
            model_int2 = self.tree_int2.get_model()
            for row in model_int2:
                if row[1]:
                    insert_query = "INSERT INTO ruta_metabolito (idmet, idruta) VALUES (%s, %s)"
                    search_query = "SELECT id FROM ruta WHERE nombre= %s"
                    cursor.execute(search_query, (row[0],))
                    idpw = cursor.fetchone()
                    data = (idobj, idpw)
                    cursor.execute(insert_query, data)
                    conn.commit()
                    
            self.on_combo_changed(self.combo_tables)

        elif table_name == "gen":
            # entry1 es nombre
            # entry2 es cromosoma
            # entry3 es locus
            # se saca la seq de self.buffer_seq

            insert_query = "INSERT INTO gen (nombre, cromosoma, locus, seq) VALUES (%s, %s, %s, %s);"
            name = self.entry1.get_text()
            chrom = self.entry2.get_text()

            try:
                chrom = int(chrom)
                if not 1 <= chrom <= 23:
                    on_error_clicked(self, principal="Error", secundaria="Cromosoma no existente")
                    return
            except:
                if chrom == "None" or chrom == "" or chrom == "NULL":
                    chrom = None
                else:
                    on_error_clicked(self, principal="Error", secundaria="El cromosoma debe ser un numero entero o nulo")
                    return
                
            locus = self.entry3.get_text()
            if locus == "None" or locus == "" or locus == "NULL":
                locus = None
            # sacar la seq

            seq = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)
            data = (name, chrom, locus, seq)
            try:
                cursor.execute(insert_query, data)
                conn.commit()
            except:
                conn.rollback()
                on_error_clicked(self, principal="Error", secundaria="El nombre del gen debe ser unico")
                return
            self.on_combo_changed(self.combo_tables)
            
        elif table_name == "enzima":
            insert_query = "INSERT INTO enzima (nombre, idgen, seq) VALUES (%s, %s, %s);"
            #entry 1 es nombre
            #entry2 es idgen
            #secuencia se saca del buffer_seq

            name = self.entry1.get_text()
            idgen = self.entry2.get_text()
            
            try:
                idgen = int(idgen)
            except:
                if idgen == "None" or idgen == "" or idgen == "NULL":
                    idgen = None
                else:
                    on_error_clicked(self, principal="Error", secundaria="id del gen debe ser entero o Nulo")
                    return

            seq = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)
            data = (name, idgen, seq)
            try:
                cursor.execute(insert_query, data)
                conn.commit()
            except:
                conn.rollback()
                on_error_clicked(self, principal="Error", secundaria="El nombre del gen debe ser unico")
                return

            # Para las tablas intermedias
            
            model_int1 = self.tree_int1.get_model()
            for row in model_int1: #row[0] es el nombre row[1] es el bool
                if row[1]:
                    insert_query = "INSERT INTO enz_metabolito (idenz, idmet) VALUES (%s, %s)"
                    search_query = "SELECT id FROM metabolito WHERE nombre = %s"
                    cursor.execute(search_query, (row[0],))
                    idmet = cursor.fetchone()
                    data = (idobj, idmet)
                    cursor.execute(insert_query, data)
                    conn.commit()
                    
            # para la otra tabla intermedia
            model_int2 = self.tree_int2.get_model()
            for row in model_int2:
                if row[1]:
                    insert_query = "INSERT INTO ruta_enzima (idenz, idruta) VALUES (%s, %s)"
                    search_query = "SELECT id FROM ruta WHERE nombre = %s"
                    cursor.execute(search_query, (row[0],))
                    idpw = cursor.fetchone()
                    data = (idobj, idpw)
                    cursor.execute(insert_query, data)
                    conn.commit()
                    
            self.on_combo_changed(self.combo_tables)

        # QUEDA PODER AÑADIR RUTAS
        elif table_name == "ruta":
            insert_query = "INSERT INTO ruta (nombre, idsmpdb, data_svg) VALUES (%s, %s, %s)"
            name = self.entry1.get_text()
            idsmpdb = self.entry2.get_text()
            try:
                idsmpdb = int(idsmpdb)
            except:
                if idsmpdb == "None" or smpdb == "" or smpdb == "NULL":
                    idgen = None
                else:
                    on_error_clicked(self, principal="Error", secundaria="id de SMPDB debe ser entero o Nulo")
                    return
            svg_data = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)
            try:
                cursor.execute(insert_query, data)
                conn.commit()
            except:
                conn.rollback()
                on_error_clicked(self, principal = "Error", secundaria = "Nombre de ruta debe ser unico")
                return

            # para las tablas intermedias - ruta metabolito
            model_int1 = self.tree_int1.get_model()
            for row in model_int1:
                if row[1]:
                    insert_query = "INSERT INTO ruta_metabolito (idruta, idmet) VALUES (%s, %s)"
                    search_query = "SELECT id FROM metabolito WHERE nombre = %s"
                    cursor.execute(search_query, (row[0],))
                    idmet = cursor.fetchone()
                    data = (idobj, idmet)
                    cursor.execute(insert_query, data)
                    conn.commit()
            # para la otra tabla intermedia - ruta_enzima
            model_int2 = self.tree_int2.get_model()
            for row in model_int2:
                if row[1]:
                    insert_query = "INSERT INTO ruta_enzima (idruta, idenz) VALUES (%s, %s)"
                    search_query = "SELECT id FROM enzima WHERE nombre = %s"
                    cursor.execute(search_query, (row[0],))
                    idenz = cursor.fetchone()
                    data = (idboj, idenz)
                    cursor.execute(insert_query, data)
                    conn.commit()
            self.on_combo_changed(self.combo_tables)
            
                
    def on_btn_edit(self, btn):
        table_name = self.combo_tables.get_active_text()
        model = self.tree.get_model()
        path = self.tree.get_cursor()[0]
        idobj = model[path][0]

        if table_name == "metabolito":
            update_query = f"UPDATE metabolito SET nombre = %s, smiles = %s, formula = %s, peso_mol = %s WHERE id = {idobj};"
            
            name = self.entry1.get_text()
            sml = self.entry2.get_text()
            formula = self.entry3.get_text()
            mw = self.sp.get_value()
            data = (name, sml, formula, mw)

            cursor.execute(update_query, data)
            conn.commit()

        elif table_name == "enzima":
            update_query = f"UPDATE enzima SET nombre = %s, seq = %s, idgen = %s WHERE id = {idobj}"
            name = self.entry1.get_text()
            idgen = self.entry2.get_text()
            
            try:
                idgen = int(idgen)
            except:
                if idgen == "None" or idgen == "" or idgen == "NULL":
                    idgen = None
                else:
                    on_error_clicked(self, principal="Error", secundaria="id del gen debe ser entero o Nulo")
                    return

            seq = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)
            data = (name, seq, idgen)
            cursor.execute(update_query, data)
            conn.commit()

        elif table_name == "gen":
            update_query = f"UPDATE gen SET nombre = %s, cromosoma = %s, locus = %s, seq = %s WHERE id = {idobj}"
            name = self.entry1.get_text()
            cromosoma = self.entry2.get_text()
            locus = self.entry2.get_text()
            seq = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)
            try:
                chrom = int(chrom)
                if not 1 <= chrom <= 23:
                    on_error_clicked(self, principal="Error", secundaria="Cromosoma no existente")
                    return
            except:
                if chrom == "None" or chrom == "" or chrom == "NULL":
                    chrom = None
                else:
                    on_error_clicked(self, principal="Error", secundaria="El cromosoma debe ser un numero entero o nulo")
                    return
                
            locus = self.entry3.get_text()
            if locus == "None" or locus == "" or locus == "NULL":
                locus = None
            data = (nombre, chrom, locus, seq)
            cursor.execute(update_query, data)
            conn.commit()

        elif table_name == "ruta":
            update_query = f"UPDATE ruta set nombre = %s, idsmpdb = %s, data_svg = %s WHERE id = {idobj}"
            name = self.entry1.get_text()
            idsmpdb = self.entry2.get_text()
            svg_data = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)
            try:
                idsmpdb = int(idsmpdb)
            except:
                if idsmpdb == "None" or smpdb == "" or smpdb == "NULL":
                    idgen = None
                else:
                    on_error_clicked(self, principal="Error", secundaria="id de SMPDB debe ser entero o Nulo")
                    return
                
            data = (name, idsmpdb, svg_data)
            cursor.execute(update_query, data)
        self.on_combo_changed(self.combo_tables)
            
    def on_btn_del(self, btn):
        table_name = self.combo_tables.get_active_text()
        model = self.tree.get_model()
        path = self.tree.get_cursor()[0]
        idobj = model[path][0]
        
        if table_name == "metabolito":

            del_statement = f"DELETE FROM enz_metabolito WHERE idmet = {idobj}; DELETE FROM ruta_metabolito WHERE idmet = {idobj}; DELETE FROM metabolito WHERE id = {idobj};"
        
        elif table_name == "enzima":
            del_statement = f"DELETE FROM enz_metabolito WHERE idenz = {idobj}; DELETE FROM enz_metabolito WHERE idenz = {idobj}; DELETE FROM enzima WHERE id = {idobj};"
            
        elif table_name == "gen":
            #primero actualizar las enzimas asociadas a ese gen a none
            update_statement = f"UPDATE enzima SET idgen = NULL WHERE idgen = {idobj};"
            cursor.execute(update_statement)
            conn.commit()
            del_statement = f"DELETE FROM gen WHERE id = {idobj};"
            
        elif table_name == "ruta":
            del_statement = f"DELETE FROM ruta_metabolito WHERE idruta = {idobj}; DELETE FROM ruta_enzima WHERE idruta = {idobj}; DELETE FROM ruta WHERE id = {idobj};"

        cursor.execute(del_statement);
        conn.commit()
        self.on_combo_changed(self.combo_tables)

        on_info_clicked(self, principal="Eliminacion de registros", secundaria="Se ha eliminado un registro")
        self.btn_del.hide()
        self.btn_add.hide()
        self.btn_edit.hide()

    def on_btn_about(self, btn):
        AboutDialog()

if __name__ == "__main__":
    window = MainWindow()
    window.show_all()
    window.btn_add.hide()
    window.btn_edit.hide()
    window.btn_del.hide()
    window.scrolledw.hide()
    # Ocultar particulares
    window.hide_widgs()

    Gtk.main()

