# Proyecto Unidad III - Base de Datos de metabolómica
# Sebastián Bustamante - Cecilia Castillo - Magdalena Lolas

# GTK
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf

# Dialogo About para informacion de los integrantes
from about_dialog import AboutDialog

# Mensajes utiles para notificar lo que suceda
from messages import on_error_clicked, on_info_clicked, on_question_clicked

import psycopg2 # Conexion a python

# Generar imagenes con las representacion SMILES
from rdkit import Chem
from rdkit.Chem import Draw

# Generar imagenes con el svg de la ruta
from cairosvg import svg2png

# Dialogo all-purpose para mostrar las imagenes, y resumenes
from ap_dialog import DialogWindow

# Procesar la salida de un proc. almacenado
import csv
from io import StringIO

# conexion a la bdd
conn = psycopg2.connect(database="bdd_final", user="postgres", password="", host="localhost", port="5432")
cursor = conn.cursor()

# Ventana principal
class MainWindow(Gtk.Window):
    def __init__(self):
        super().__init__()
        self.maximize()
        self.set_border_width(20)
        self.connect("destroy", Gtk.main_quit)

        # Se crea la grid que contendrá los demas widgets
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        # Se crea la headerbar
        self.hb = Gtk.HeaderBar()
        self.hb.set_title("Base de datos de metabolomica")
        self.hb.set_subtitle("Interfaz gráfica")
        self.hb.set_show_close_button(True)
        self.set_titlebar(self.hb)

        # Boton Ventana About
        self.btn_about = Gtk.Button()
        icon = Gio.ThemedIcon(name="help-about")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_about.add(image)
        self.btn_about.connect("clicked", self.on_btn_about)
        self.hb.pack_end(self.btn_about)

        # Scrolledwindow para el Treeview
        self.scrolledw = Gtk.ScrolledWindow()
        self.scrolledw.set_max_content_height(20)
        self.grid.attach(self.scrolledw, 0, 0, 10, 30)

        # Treeview principal
        self.tree = Gtk.TreeView();
        self.tree.connect("row_activated", self.on_tree_row_activated)
        self.tree.set_activate_on_single_click(True)
        self.tree.set_enable_search(True)
        self.scrolledw.add(self.tree)

        # ComboboxText para las tablas
        self.combo_tables = Gtk.ComboBoxText()
        table_names = ["metabolito", "enzima", "gen", "ruta"]
        for name in table_names:
            self.combo_tables.append_text(name)
        self.combo_tables.connect("changed", self.on_combo_changed)
        self.hb.pack_start(self.combo_tables)

        # Widgets particulares, se usan dependiendo de la tabla con la que se trabaje
        
        # Imagen para los metabolitos y rutas
        self.img = Gtk.Image()
        
        # Label1
        self.lbl1 = Gtk.Label(label="Label 1")
        self.grid.attach_next_to(self.lbl1, self.scrolledw, Gtk.PositionType.RIGHT, 1, 1)

        # Entrada 1
        self.entry1 = Gtk.Entry()
        self.entry1.connect("changed", self.on_widg_changed)
        self.grid.attach_next_to(self.entry1, self.lbl1, Gtk.PositionType.RIGHT, 1, 1)

        # Label2
        self.lbl2 = Gtk.Label(label="Label 2")
        self.grid.attach_next_to(self.lbl2, self.lbl1, Gtk.PositionType.BOTTOM, 1, 1)

        # Entrada2
        self.entry2 = Gtk.Entry()
        self.entry2.connect("changed", self.on_widg_changed)
        self.grid.attach_next_to(self.entry2, self.lbl2, Gtk.PositionType.RIGHT, 1, 1)

        # Label3
        self.lbl3 = Gtk.Label(label="Label 3")
        self.grid.attach_next_to(self.lbl3, self.lbl2, Gtk.PositionType.BOTTOM, 1, 1)

        # Entrada3
        self.entry3 = Gtk.Entry()
        self.entry3.connect("changed", self.on_widg_changed)
        self.grid.attach_next_to(self.entry3, self.lbl3, Gtk.PositionType.RIGHT, 1, 1)
        
        # Label4 (siempre va al lado del spinbutton)
        self.lbl4 = Gtk.Label(label="Label 4")
        self.grid.attach_next_to(self.lbl4, self.lbl3, Gtk.PositionType.BOTTOM, 1, 1)

        # SpinButton para floats
        self.sp = Gtk.SpinButton()
        self.sp.set_range(0, 1000)
        self.sp.configure(None, 1.00, 3)
        self.sp.set_numeric(False)
        self.sp.set_numeric(True)
        self.sp.set_increments(1, 5)
        self.sp.connect("changed", self.on_widg_changed)
        self.grid.attach_next_to(self.sp, self.lbl4, Gtk.PositionType.RIGHT, 1, 1)

        # Textview para las secuencias de genes, enzimas y svg de la ruta
        self.textview = Gtk.TextView()
        self.buffer_seq = self.textview.get_buffer()
        self.buffer_seq.connect("changed", self.on_widg_changed)
        self.textview.set_wrap_mode(Gtk.WrapMode.CHAR)
        
        # Label para las secuencias de genes, enzimas y svg de la ruta
        self.scrolledw_text = Gtk.ScrolledWindow()
        self.scrolledw_text.set_max_content_height(20)
        self.grid.attach_next_to(self.scrolledw_text, self.lbl4, Gtk.PositionType.BOTTOM, 10, 5)
        self.lbl_text = Gtk.Label(label = "Secuencia")
        self.grid.attach_next_to(self.lbl_text, self.scrolledw_text, Gtk.PositionType.TOP, 1, 1)
        self.scrolledw_text.add(self.textview)

        # Scrolled window para la tabla intermedia 1
        self.scrolledw_int1 = Gtk.ScrolledWindow()
        self.tree_int1 = Gtk.TreeView()
        self.tree_int1.set_activate_on_single_click(True)
        self.tree_int1.set_enable_search(True)
        self.scrolledw_int1.add(self.tree_int1)
        self.grid.attach_next_to(self.scrolledw_int1, self.scrolledw_text, Gtk.PositionType.BOTTOM, 5, 10)

        # Scrolled Window para la tabla intermedia 2
        self.scrolledw_int2 = Gtk.ScrolledWindow()
        self.tree_int2 = Gtk.TreeView()
        self.tree_int2.set_activate_on_single_click(True)
        self.tree_int2.set_enable_search(True)
        self.scrolledw_int2.add(self.tree_int2)
        self.grid.attach_next_to(self.scrolledw_int2, self.scrolledw_int1, Gtk.PositionType.RIGHT, 20, 10)

        # Toggle para la el arbol de la tabla intermedia 1
        self.celltoggle1 = Gtk.CellRendererToggle()
        self.celltoggle1.set_activatable(True)
        self.celltoggle1.connect("toggled", self.on_celltoggle_clicked)

        # Toggle para el arbol de la tabla intermedia 2
        self.celltoggle2 = Gtk.CellRendererToggle()
        self.celltoggle2.set_activatable(True)
        self.celltoggle2.connect("toggled", self.on_celltoggle_clicked)

        # Treeview resumen, para la composicion completa de una ruta
        self.tree_summ = Gtk.TreeView()

        # Boton para mostrar las imagenes
        self.btn_img = Gtk.Button()
        self.btn_img_box = Gtk.Box()
        icon = Gio.ThemedIcon(name="emblem-favorite")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_img_box.add(image)
        self.btn_img.add(self.btn_img_box)
        self.grid.attach_next_to(self.btn_img, self.entry1, Gtk.PositionType.RIGHT, 1, 1)
        self.btn_img.connect("clicked", self.on_btn_img)

        # Boton para resumenes, acciones e informaciones importantes
        self.btn_summ = Gtk.Button()
        self.btn_summ_box = Gtk.Box()
        icon = Gio.ThemedIcon(name="emblem-favorite")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_summ_box.add(image)
        self.btn_summ.add(self.btn_summ_box)
        self.grid.attach_next_to(self.btn_summ, self.entry2, Gtk.PositionType.RIGHT, 1, 1)
        self.btn_summ.connect("clicked", self.on_btn_summ)

        # Boton para añadir registros
        self.btn_add = Gtk.Button()
        self.btn_add_box = Gtk.Box()
        icon = Gio.ThemedIcon(name="gtk-add")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_add_box.add(image)
        self.lbl_btn_add = Gtk.Label(label="ADD")
        self.btn_add_box.add(self.lbl_btn_add)
        self.btn_add_box.set_spacing(5)
        self.btn_add.add(self.btn_add_box)
        self.grid.attach_next_to(self.btn_add, self.scrolledw, Gtk.PositionType.BOTTOM, 1, 1)
        #self.grid.attach(self.btn_add, 0, 2, 1, 1)
        self.btn_add.connect("clicked", self.on_btn_add)
        self.btn_add.set_sensitive(False)
        
        # Boton para editar registros
        self.btn_edit = Gtk.Button()
        self.btn_edit_box = Gtk.Box()
        icon = Gio.ThemedIcon(name="gtk-edit")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_edit_box.add(image)
        self.lbl_btn_edit = Gtk.Label(label="EDIT")
        self.btn_edit_box.add(self.lbl_btn_edit)
        self.btn_edit_box.set_spacing(5)
        self.btn_edit.add(self.btn_edit_box)
        self.grid.attach_next_to(self.btn_edit, self.btn_add, Gtk.PositionType.RIGHT, 1, 1)
        self.btn_edit.connect("clicked", self.on_btn_edit)
        self.btn_edit.set_sensitive(False)
        
        # Boton para eliminar registros
        self.btn_del = Gtk.Button()
        self.btn_del_box = Gtk.Box()
        icon = Gio.ThemedIcon(name="gtk-delete")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_del_box.add(image)
        self.lbl_btn_del = Gtk.Label(label="DELETE")
        self.btn_del_box.add(self.lbl_btn_del)
        self.btn_del_box.set_spacing(5)
        self.btn_del.add(self.btn_del_box)
        self.grid.attach_next_to(self.btn_del, self.btn_edit, Gtk.PositionType.RIGHT, 1, 1)
        self.btn_del.connect("clicked", self.on_btn_del)
        self.btn_del.set_sensitive(False)


    # Manejo de sensibilidad de botones
    def on_widg_changed(self, widg):
        self.btn_del.set_sensitive(False)
        self.btn_edit.set_sensitive(True)
        self.btn_add.set_sensitive(True)

    # Llenar el treeview principal
    def make_treeview(self, records, columns, tree):

        if tree.get_columns():
            for column in tree.get_columns():
                tree.remove_column(column)

        column_length = len(columns)
        tmodel = Gtk.ListStore(*(column_length*[str]))
        tree.set_model(model=tmodel)

        cell = Gtk.CellRendererText()

        for item in range(len(columns)):
            column = Gtk.TreeViewColumn(columns[item][0], cell, text=item)
            tree.append_column(column)
            column.set_sort_column_id(item)
        for item in records:
            line = [str(x) for x in item]
            tmodel.append(line)

    # Llenar el treeview para las tablas intermedias
    def make_int_treeview(self, records, columns, tree, celltoggle):
        if tree.get_columns():
            for column in tree.get_columns():
                tree.remove_column(column)

        column_length = len(columns)
        tmodel = Gtk.ListStore(str, bool)
        tree.set_model(model=tmodel)

        celltext = Gtk.CellRendererText()
        
        column_text = Gtk.TreeViewColumn(columns[0], celltext, text=0)
        column_text.set_sort_column_id(0)
        
        column_toggle = Gtk.TreeViewColumn(columns[1], celltoggle, active=1)
        column_toggle.set_sort_column_id(1)
        
        tree.append_column(column_text)
        tree.append_column(column_toggle)

        for item in records:
            text = str(item[0])
            checked = int(item[1])
            tmodel.append([text, checked])

    # Evento cuando se clickea una checkbox
    def on_celltoggle_clicked(self, toggle, path):

        # Determinar con que arbol se trabaja
        if toggle is self.celltoggle1:
            tree = self.tree_int1
        else:
            tree = self.tree_int2
            
        past = toggle.get_active()
        model = tree.get_model()

        print(model[path][1])
        model[path][1] = not past # Cambiar el estado
        tree.set_model(model)

        # Habilitar botones pues se han hecho cambios
        self.btn_add.set_sensitive(True)
        self.btn_edit.set_sensitive(True)

    # Sacar la informacion de la tabla con la que se trabaja, nombre de columnas y registros
    def get_table_data(self, table_name):
        cursor.execute(f"SELECT column_name FROM information_schema.columns WHERE table_name = '{table_name}' order by ordinal_position;")
        columns = cursor.fetchall()

        cursor.execute(f"SELECT * FROM {table_name};")
        records = cursor.fetchall()

        return records, columns

    # Evento por cada fila clickeada del arbol principal
    def on_tree_row_activated(self, tree, path, col):
        # Determinar cual es la tabla con la que se trabaja
        table = self.combo_tables.get_active_text()
        model = tree.get_model()
        
        self.btn_add.show()
        self.btn_edit.show()
        self.btn_del.show()

        #Dependiendo de la tabla de muestran unos u otros widgets, con diferentes valores
        if table == "metabolito":

            # No se muestra el boton para imagen si no hay imagen
            smile = model[path][2] # Estructura smiles
            try:
                m = Chem.MolFromSmiles(smile)
                Draw.MolToFile(m, "mol.png")
                self.img.set_from_file("mol.png")
                self.btn_img.show()
            except:
                self.btn_img.hide()


            self.lbl1.set_label("Nombre")
            self.entry1.set_text(model[path][1])
            
            self.lbl2.set_label("Smiles")
            self.entry2.set_text(model[path][2])
            
            self.lbl3.set_label("Formula")
            self.entry3.set_text(model[path][3])

            self.lbl4.set_label("Peso molecular")
            self.sp.set_value(float(model[path][4]))

            met_id = model[path][0]            
            
            # Relaciones de enzima con metabolito - Tabla intermedia 1
            cursor.execute("SELECT nombre FROM enzima");
            enz_names = cursor.fetchall()
            search_query = f"SELECT enzima.nombre as nombre_enz FROM enz_metabolito INNER JOIN metabolito ON enz_metabolito.idmet = metabolito.id INNER JOIN enzima ON enz_metabolito.idenz = enzima.id WHERE enz_metabolito.idmet = {met_id}";
            cursor.execute(search_query)
            enz_con = cursor.fetchall()
            final_regist = self.get_treeint_data(enz_con, enz_names)
            self.make_int_treeview(final_regist, ["Enzima","Relacionado"], self.tree_int1, self.celltoggle1)

            # Relaciones de metabolito con ruta - Tabla intermedia 2
            cursor.execute("SELECT nombre FROM ruta");
            pw_names = cursor.fetchall()
            search_query = f"SELECT ruta.nombre as nombre_ruta FROM ruta_metabolito INNER JOIN metabolito ON ruta_metabolito.idmet = metabolito.id INNER JOIN ruta ON ruta_metabolito.idruta = ruta.id WHERE ruta_metabolito.idmet = {met_id}";
            cursor.execute(search_query)
            pw_con = cursor.fetchall()
            final_pws = self.get_treeint_data(pw_con, pw_names)

            self.make_int_treeview(final_pws, ["Ruta", "Relacionado"], self.tree_int2, self.celltoggle2)
            
            self.show_met()
            
        elif table == "gen":

            self.lbl1.set_label("Nombre")
            self.entry1.set_text(model[path][1])
            
            self.lbl2.set_label("Cromosoma")
            self.entry2.set_text(model[path][3])
            
            self.lbl3.set_label("Locus")
            self.entry3.set_text(model[path][4])

            self.lbl_text.set_label("Secuencia")
            self.buffer_seq.set_text(model[path][2])

            self.show_gene()
            
        elif table == "enzima":

            self.lbl1.set_label("Nombre")
            self.entry1.set_text(model[path][1])
            
            self.lbl2.set_label("idgen")
            self.entry2.set_text(model[path][3])

            self.lbl_text.set_label("Secuencia")
            self.buffer_seq.set_text(model[path][2])
            
            idenz = model[path][0]

            # Union con la tabla enz_metabolito - Intermedia 1
            cursor.execute("SELECT nombre FROM metabolito")
            met_names = cursor.fetchall()
            search_query = f"SELECT metabolito.nombre as nombre_met FROM enz_metabolito INNER JOIN metabolito ON enz_metabolito.idmet = metabolito.id INNER JOIN enzima ON enz_metabolito.idenz = enzima.id WHERE enz_metabolito.idenz = {idenz}";
            cursor.execute(search_query)
            met_con = cursor.fetchall()
            final_regist = self.get_treeint_data(met_con, met_names)
            self.make_int_treeview(final_regist, ["Metabolito", "Relacionado"], self.tree_int1, self.celltoggle1)
            
            # Union con la tabla ruta_enzima - Intermedia 2
            cursor.execute("SELECT nombre FROM ruta")
            pw_names = cursor.fetchall()
            search_query = f"SELECT ruta.nombre as nombre_ruta FROM ruta_enzima INNER JOIN ruta ON ruta_enzima.idruta = ruta.id INNER JOIN enzima ON ruta_enzima.idenz = enzima.id WHERE ruta_enzima.idenz = {idenz}"
            cursor.execute(search_query)
            pw_con = cursor.fetchall()
            final_regist = self.get_treeint_data(pw_con, pw_names)
            self.make_int_treeview(final_regist, ["Ruta", "Relacionado"], self.tree_int2, self.celltoggle2)
            
            self.show_enz()
            
        elif table == "ruta":
            
            svg_data = model[path][3]
            idpw = model[path][0]
            
            try:
                svg2png(bytestring=svg_data, write_to="pathway.png")
                self.img.set_from_file("pathway.png")
                self.btn_img.show()
            except:
                self.btn_img.hide()

            cursor.execute(f"SELECT comp_ruta({idpw})")
            data = cursor.fetchall()
            
            columns = [("Ruta",), ("Metabolito",), ("Enzima",)]

            clean_data = []
            for row in data:
                clean_data.append(self.parse_input_string(row[0]))

            if len(clean_data) > 0:
                self.make_treeview(clean_data, columns, self.tree_summ)
                self.btn_summ.show()
            else:
                self.btn_summ.hide()

            self.lbl1.set_label("Nombre")
            self.entry1.set_text(model[path][1])

            self.lbl2.set_label("ID SMPDB")
            self.entry2.set_text(model[path][2])

            self.lbl_text.set_label("Data del svg")
            self.buffer_seq.set_text(model[path][3])
            
            
            idpw = model[path][0]

            # Union con la tabla ruta_metabolito - Intermedia 1

            cursor.execute("SELECT nombre FROM metabolito")
            met_names = cursor.fetchall()
            search_query = f"SELECT metabolito.nombre as nombre_met FROM ruta_metabolito INNER JOIN metabolito ON ruta_metabolito.idmet = metabolito.id INNER JOIN ruta ON ruta_metabolito.idruta = ruta.id WHERE ruta_metabolito.idruta = {idpw}"
            cursor.execute(search_query)
            met_con = cursor.fetchall()
            final_regist = self.get_treeint_data(met_con, met_names)
            self.make_int_treeview(final_regist, ["Metabolito", "Relacionado"], self.tree_int1, self.celltoggle1)
            
            # Union con la tabla ruta_enzima - Intermedia 2
            cursor.execute("SELECT nombre FROM enzima")
            enz_names = cursor.fetchall()
            search_query = f"SELECT enzima.nombre as nombre_enz FROM ruta_enzima INNER JOIN ruta ON ruta_enzima.idruta = ruta.id INNER JOIN enzima ON ruta_enzima.idenz = enzima.id WHERE ruta_enzima.idruta = {idpw}"
            cursor.execute(search_query)
            enz_con = cursor.fetchall()
            final_regist = self.get_treeint_data(enz_con, enz_names)
            self.make_int_treeview(final_regist, ["Enzima","Relacionado"], self.tree_int2, self.celltoggle2)
            
            self.show_pw()
            
        self.btn_del.set_sensitive(True)
        self.btn_edit.set_sensitive(False)
        self.btn_add.set_sensitive(False)

    # Definiar la informacion (cuales True, cuales False) para la tabla intermedia
    def get_treeint_data(self, con, full):
        final = []
        for regist in full:
            name = regist[0]
            if con:
                if self.check_existence(con, name):
                    final.append((name, True))
                else:
                    final.append((name, False))
            else:
                final.append((name, False))
        return final

    # Revisar la existencia de un nombre x dentro de el conjunto de nombres que si presentan coneccion
    def check_existence(self, con, name):
        for tupl in con:
            real_name = tupl[0]
            if real_name == name:
                return True
        return False

    # Evento por cada vez que se cambia la tabla en la ComboBoxText
    def on_combo_changed(self, combo):

        table = combo.get_active_text()
        records, columns = self.get_table_data(table)
        self.make_treeview(records, columns, self.tree)
        self.hide_widgs()

        # Se muestras algunas columnas mientras otras se ocultan, aunque igual se conservan dentro del modelo
        # permite facil acceso a ellas en caso de ser necesario

        if table == "metabolito":
            col = self.tree.get_column(2)
            col.set_visible(False)
            col = self.tree.get_column(3)
            col.set_visible(False)
            col = self.tree.get_column(4)
            col.set_visible(False)
            
        elif table == "enzima":
            col = self.tree.get_column(2)
            col.set_visible(False)
            self.btn_summ.set_label("Limpiar")
            
        elif table == "gen":
            col = self.tree.get_column(2)
            col.set_visible(False)
            pass
        elif table == "ruta":
            col = self.tree.get_column(3)
            col.set_visible(False)
            self.btn_summ.set_label("Resumen")

        self.btn_add.show()
        self.btn_edit.show()
        self.btn_del.show()
        self.btn_del.set_sensitive(False)
        self.scrolledw.show()

    # Funciones que muestran algunos widgets especificos dependiendo de la tabla
    
    def show_pw(self):

        self.lbl1.show()
        self.entry1.show()

        self.lbl2.show()
        self.entry2.show()

        self.scrolledw_int1.show()
        self.scrolledw_int2.show()

        self.lbl_text.show()
        self.scrolledw_text.show()
        
    def show_enz(self):

        self.lbl1.show()
        self.entry1.show()

        self.lbl2.show()
        self.entry2.show()

        self.scrolledw_int1.show()
        self.scrolledw_int2.show()
        
        self.scrolledw_text.show()
        self.lbl_text.show()

        self.btn_summ.show()
        
    def show_gene(self):
        
        self.lbl1.show()
        self.entry1.show()

        self.lbl2.show()
        self.entry2.show()

        self.lbl3.show()
        self.entry3.show()

        self.lbl_text.show()
        self.scrolledw_text.show()

    def show_met(self):

        self.lbl1.show()
        self.entry1.show()
        
        self.lbl2.show()
        self.entry2.show()
        
        self.lbl3.show()
        self.entry3.show()
        
        self.lbl4.show()
        self.sp.show()

        self.scrolledw_int1.show()
        self.scrolledw_int2.show()
        
    def hide_widgs(self):
        self.lbl1.hide()
        self.lbl2.hide()
        self.lbl3.hide()
        self.lbl4.hide()

        self.entry1.hide()
        self.entry2.hide()
        self.entry3.hide()
        self.sp.hide()

        self.scrolledw_text.hide()
        self.lbl_text.hide()

        self.btn_img.hide()
        self.btn_summ.hide()
        
        self.scrolledw_int1.hide()
        self.scrolledw_int2.hide()

    # Revisar el nombre, ya sea de ruta, metabolito, enzima, o gen
    # todos deben ser unicos, (ademas se agrega que no null)
    def check_name(self, name):
        if name == "" or name == "None" or name == "NULL":
            on_error_clicked(self, principal="Error", secundaria="No se puede tener nombre nulo")
            return False
        return True

    # Intentar insertar en una tabla con nombres unicos
    def try_insert_unique(self, insert_query, data):
        try:
            cursor.execute(insert_query, data)
            conn.commit()
        except: # falla cuando ya se repite el nombre
            conn.rollback()
            on_error_clicked(self, principal="Error", secundaria="El nombre debe ser unico")
            return

    # Inserta en una tabla intermedia
    def try_insert_int_table(self, model_int, insert_query, search_query, idobj):
        for row in model_int:
            if row[1]:
                cursor.execute(search_query, (row[0],))
                idcon = cursor.fetchone()
                data = (idobj, idcon)
                cursor.execute(insert_query, data)
                conn.commit()

    # Revisa la validez del cromosoma
    def check_chrom(self, chrom):
        try:
            chrom = int(chrom)
            if not 1 <= chrom <= 23:
                on_error_clicked(self, principal="Error", secundaria="Cromosoma no valido")
            else:
                return chrom
        except:
            if chrom == "" or chrom == "None" or chrom == "NULL":
                return None
            else:
                on_error_clicked(self, principal="Error", secundaria="Cromosoma debe ser numero entero o nulo")
        return -1

    # Revisa numeros, como el idgen o el idsmpdb, cromosoma no porque tiene limites superior e inferior
    def check_num(self, num_str):
        try:
            num_str = int(num_str)
        except:
            if num_str == "None" or num_str == "" or num_str == "NULL":
                num_str = None
            else:
                on_error_clicked(self, principal="Error", secundaria="Valor numerico debe ser entero o nulo")
                num_str = -1
        return num_str
    
    # Añadir registros            
    def on_btn_add(self, btn):
        table_name = self.combo_tables.get_active_text()
        model = self.tree.get_model()
        path = self.tree.get_cursor()[0]
        idobj = model[path][0]
        principal = "Error"

        if table_name == "metabolito":
            insert_query = "INSERT INTO metabolito (nombre, smiles, formula, peso_mol) VALUES (%s, %s, %s, %s)"
            name = self.entry1.get_text()
            sml = self.entry2.get_text()
            formula = self.entry3.get_text()
            mw = self.sp.get_value()
            data = (name, sml, formula, mw)

            if not self.check_name(name):
                return
            self.try_insert_unique(insert_query, data)

            # Las cosas de las tablas intermedias
            # 1 es enz_metabolito, 2 es ruta_metabolito
            model_int1 = self.tree_int1.get_model()
            insert_query = "INSERT INTO enz_metabolito (idmet, idenz) VALUES (%s, %s)"
            search_query = "SELECT id FROM enzima WHERE nombre = %s"
            self.try_insert_int_table(model_int1, insert_query, search_query, idobj)
            
            # Para la otra tabla intermedia
            model_int2 = self.tree_int2.get_model()
            insert_query = "INSERT INTO ruta_metabolito (idmet, idruta) VALUES (%s, %s)"
            search_query = "SELECT id FROM ruta WHERE nombre= %s"
            self.try_insert_int_table(model_int2, insert_query, search_query, idobj)

        elif table_name == "gen":

            insert_query = "INSERT INTO gen (nombre, cromosoma, locus, seq) VALUES (%s, %s, %s, %s);"
            name = self.entry1.get_text()
            chrom = self.entry2.get_text()

            if not self.check_name(name):
                return

            chrom = self.check_chrom(chrom)
            if chrom == -1:
                return
            
            locus = self.entry3.get_text()
            if locus == "None" or locus == "" or locus == "NULL":
                locus = None
                
            seq = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)
            data = (name, chrom, locus, seq)
            self.try_insert_unique(insert_query, data)
            
        elif table_name == "enzima":
            insert_query = "INSERT INTO enzima (nombre, idgen, seq) VALUES (%s, %s, %s);"

            name = self.entry1.get_text()
            if not self.check_name(name):
                return

            idgen = self.entry2.get_text()
            idgen = self.check_num(idgen)
            if idgen == -1:
                return
            
            seq = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)
            data = (name, idgen, seq)
            self.try_insert_unique(insert_query, data)
            
            # Para las tablas intermedias
            
            model_int1 = self.tree_int1.get_model()
            insert_query = "INSERT INTO enz_metabolito (idenz, idmet) VALUES (%s, %s)"
            search_query = "SELECT id FROM metabolito WHERE nombre = %s"
            self.try_insert_int_table(model_int1, insert_query, search_query, idobj)
            
            # para la otra tabla intermedia
            model_int2 = self.tree_int1.get_model()
            insert_query = "INSERT INTO ruta_enzima (idenz, idruta) VALUES (%s, %s)"
            search_query = "SELECT id FROM ruta WHERE nombre = %s"
            self.try_insert_unique(insert_query, data)

        elif table_name == "ruta":
            insert_query = "INSERT INTO ruta (nombre, idsmpdb, data_svg) VALUES (%s, %s, %s)"
            name = self.entry1.get_text()

            if not self.check_name(name):
                return
            
            idsmpdb = self.entry2.get_text()
            idsmpdb = self.check_num(idsmpdb)
            if idsmpb == -1:
                return
            
            svg_data = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)
            data = (name, idsmpdb, svg_data)
            self.try_insert_unique(insert_query, data)

            # Para las tablas intermedias - ruta metabolito
            model_int1 = self.tree_int1.get_model()
            insert_query = "INSERT INTO ruta_metabolito (idruta, idmet) VALUES (%s, %s)"
            search_query = "SELECT id FROM metabolito WHERE nombre = %s"
            self.try_insert_int_table(model_int1, insert_query, search_query, idobj)
            
            # Para la otra tabla intermedia - ruta_enzima
            model_int2 = self.tree_int2.get_model()
            insert_query = "INSERT INTO ruta_enzima (idruta, idenz) VALUES (%s, %s)"
            search_query = "SELECT id FROM enzima WHERE nombre = %s"
            self.try_insert_int_table(model_int2, insert_query, search_query, idobj)

        # Notificiar que se hicieron cambios y quitar la sensibilidad de los botones
        on_info_clicked(self, principal="Añadir un registro", secundaria="Se ha añadido un registro")
        self.btn_edit.set_sensitive(False)
        self.btn_add.set_sensitive(False)
        self.on_combo_changed(self.combo_tables)
            
    # Editar registros
    def on_btn_edit(self, btn):
        table_name = self.combo_tables.get_active_text()
        model = self.tree.get_model()
        path = self.tree.get_cursor()[0]
        idobj = model[path][0]

        if table_name == "metabolito":
            update_query = f"UPDATE metabolito SET nombre = %s, smiles = %s, formula = %s, peso_mol = %s WHERE id = {idobj};"
            
            name = self.entry1.get_text()
            sml = self.entry2.get_text()
            formula = self.entry3.get_text()
            mw = self.sp.get_value()
            data = (name, sml, formula, mw)

            if not self.check_name(name):
                return
            
            cursor.execute(update_query, data)
            conn.commit()

        elif table_name == "enzima":
            update_query = f"UPDATE enzima SET nombre = %s, seq = %s, idgen = %s WHERE id = {idobj}"
            name = self.entry1.get_text()
            idgen = self.entry2.get_text()

            if not self.check_name(name):
                return

            idgen = self.check_num(idgen)
            if idgen == 1:
                return
            
            seq = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)
            data = (name, seq, idgen)
            cursor.execute(update_query, data)
            conn.commit()

        elif table_name == "gen":
            update_query = f"UPDATE gen SET nombre = %s, cromosoma = %s, locus = %s, seq = %s WHERE id = {idobj}"
            name = self.entry1.get_text()
            chrom = self.entry2.get_text()
            locus = self.entry2.get_text()
            seq = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)

            if not self.check_name(name):
                return
            
            chrom = self.check_chrom(chrom)
            if chrom == -1:
                return
                            
            locus = self.entry3.get_text()
            if locus == "None" or locus == "" or locus == "NULL":
                locus = None
            
            data = (name, chrom, locus, seq)
            cursor.execute(update_query, data)
            conn.commit()

        elif table_name == "ruta":
            update_query = f"UPDATE ruta set nombre = %s, idsmpdb = %s, data_svg = %s WHERE id = {idobj}"
            name = self.entry1.get_text()
            idsmpdb = self.entry2.get_text()
            svg_data = self.buffer_seq.get_text(self.buffer_seq.get_start_iter(), self.buffer_seq.get_end_iter(), True)
            if not self.check_name(name):
                return
            
            idsmpdb = self.check_num(idsmpdb)
            if idsmpdb == -1:
                return
                            
            data = (name, idsmpdb, svg_data)
            cursor.execute(update_query, data)

        # Notificar el cambio y volver a un estado base
        on_info_clicked(self, principal="Editar un registro", secundaria="Se ha editado un registro")
        self.btn_edit.set_sensitive(False)
        self.btn_add.set_sensitive(False)
        self.on_combo_changed(self.combo_tables)

    # Eliminar registros
    def on_btn_del(self, btn):
        table_name = self.combo_tables.get_active_text()
        model = self.tree.get_model()
        path = self.tree.get_cursor()[0]
        idobj = model[path][0]

        # Considerando las conexiones entre tablas, se elimina el registro de su tabla nativa y todos aquellos otros registros donde esté presente
        
        if table_name == "metabolito":

            del_statement = f"DELETE FROM enz_metabolito WHERE idmet = {idobj}; DELETE FROM ruta_metabolito WHERE idmet = {idobj}; DELETE FROM metabolito WHERE id = {idobj};"
        
        elif table_name == "enzima":
            del_statement = f"DELETE FROM enz_metabolito WHERE idenz = {idobj}; DELETE FROM enz_metabolito WHERE idenz = {idobj}; DELETE FROM enzima WHERE id = {idobj};"
            
        elif table_name == "gen":
            # Primero actualizar las enzimas asociadas a ese gen a none
            update_statement = f"UPDATE enzima SET idgen = NULL WHERE idgen = {idobj};"
            cursor.execute(update_statement)
            conn.commit()
            del_statement = f"DELETE FROM gen WHERE id = {idobj};"
            
        elif table_name == "ruta":
            del_statement = f"DELETE FROM ruta_metabolito WHERE idruta = {idobj}; DELETE FROM ruta_enzima WHERE idruta = {idobj}; DELETE FROM ruta WHERE id = {idobj};"

        # Hacer cambios y volver a estado base
        cursor.execute(del_statement);
        conn.commit()
        self.on_combo_changed(self.combo_tables)
        on_info_clicked(self, principal="Eliminacion de registros", secundaria="Se ha eliminado un registro")
        self.btn_del.set_sensitive(False)

    # Procesar la salida del procedimiento almacenado y armar una tupla
    def parse_input_string(self, input_str):
        csv_reader = csv.reader(StringIO(input_str[1:-1]))
        parts = next(csv_reader)
        parts = [p.strip('"') if '"' in p else p for p in parts]
        return tuple(parts)

    # Boton de resumen y para el procedimiento de eliminar enzimas con gen desconocido
    def on_btn_summ(self, btn):
        label = btn.get_label()
        # Dependiendo de la label se hara una cosa o la otra

        if label == "Limpiar":
            cursor.execute("SELECT limpiar_enzimas()")
            on_info_clicked(self, secundaria="Eliminadas las enzimas con idgen Nulo", principal="Procedimiento almacenado ejecutado")
            self.on_combo_changed(self.combo_tables)

        elif label == "Resumen":
   
            dialog = DialogWindow(self, self.tree_summ)
            resp = dialog.run()
            dialog.destroy()

    # Mostrar la imagen ya sea de metabolito o ruta, mediante ventana de dialogo
    def on_btn_img(self, btn):
        dialog = DialogWindow(self, self.img)
        resp = dialog.run()
        dialog.destroy()
        
    # Abrir la ventana about 
    def on_btn_about(self, btn):
        AboutDialog()

if __name__ == "__main__":
    window = MainWindow()
    window.show_all()
    window.btn_add.hide()
    window.btn_edit.hide()
    window.btn_del.hide()
    window.scrolledw.hide()
    # Ocultar particulares
    window.hide_widgs()
    Gtk.main()

