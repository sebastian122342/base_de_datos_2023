# Ventana de dialogo para mostrar un widget entregado en una scrolledwindow

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf

class DialogWindow(Gtk.Dialog):
    def __init__(self, parent, widg):
        super().__init__(title="Dialogo", transient_for=parent, flags=0)
        self.add_button(
            Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        self.set_modal(True)
        self.maximize()
        self.box = self.get_content_area()
        self.scrolledw = Gtk.ScrolledWindow()
        
        self.box.pack_start(self.scrolledw, True, True, 0)
        self.scrolledw.add(widg)
        
        self.show_all()
