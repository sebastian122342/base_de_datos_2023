# insertar las imagenes de las rutas

# ya se tienen todas las rutas en la bdd, pero la img es null hasta el momento
# entonces se recorren todas las rutas de la bdd, se busca si es que tienen una ruta dibujada
# en el archivo de rutas y se inserta, si es que no tienen entonces no nomas

import psycopg2
from pathlib import Path

def ls(path):
    """ Se listan todos los elementos de una carpeta """
    return [obj.name for obj in Path(path).iterdir() if obj.is_file()]

def get_all_text(img_path):
    with open(img_path, "r") as svg_img:
        svg_data = svg_img.read()
        return svg_data

def get_id_from_img_name(img_name):
    # a cada nombre hay que quitarle la extension y reconvertir el numero a int
    real_id = int(img_name.strip("PW").strip(".svg"))
    return real_id

def main():

    # listar las pathways del archivo
    pathw_dir = "/home/seba/Downloads/datos_bdd/smpdb_svg"
    images = ls(pathw_dir)
   
    # conexion a la bdd
    conn = psycopg2.connect(database = "bdd_final", user = "postgres", password = "122342", host = "localhost", port = "5432")
    cursor = conn.cursor()

    for image in images:
        img_path = pathw_dir + "/" + image
        img_id = get_id_from_img_name(image)
        svg_data = get_all_text(img_path)
        cursor.execute("UPDATE ruta SET data_svg = %s WHERE idsmpdb = %s", (svg_data, img_id))
        conn.commit()

if __name__ == "__main__":
    main()
